import random
import numpy as np

def get_LFP_Name():
    return ["LFP"]

def get_LFP_color():
    return ["#ff0000"]

def get_Pulse_Names():
    return ['FR_P','FR_Pp','FR_GAs',]

def get_PPS_Names():
    return ['PSP_P','PSP_Pp','PSP_GAs',]

def get_Colors():
    return ['#ff0000','#ff0000','#0000ff',]

def get_ODE_solver():
    return ["derivT"]

def get_ODE_solver_Time():
    return ["deriv_Time"]

def get_Variable_Names():
    return ['H_P','T_P','e0_P','v0_P','r_P','H_Pp','T_Pp','e0_Pp','v0_Pp','r_Pp','H_GAs','T_GAs','e0_GAs','v0_GAs','r_GAs','m_N','s_N','C_P_to_Pp','C_Pp_to_P','C_P_to_GAs','C_GAs_to_P',]

class Model:
    def __init__(self,):
        self.v0_P=6.0
        self.e0_P=5.0
        self.r_P=0.56
        self.H_P=5.0
        self.T_P=100.0
        self.FR_P= 0.
        self.PSP_P= 0.
        self.v0_Pp=6.0
        self.e0_Pp=5.0
        self.r_Pp=0.56
        self.H_Pp=5.0
        self.T_Pp=100.0
        self.FR_Pp= 0.
        self.PSP_Pp= 0.
        self.v0_GAs=6.0
        self.e0_GAs=5.0
        self.r_GAs=0.56
        self.H_GAs=20.0
        self.T_GAs=30.0
        self.FR_GAs= 0.
        self.PSP_GAs= 0.
        self.m_N=90.0
        self.s_N=30.0
        self.noise_var_N=0.
        self.C_P_to_Pp=135.0
        self.C_Pp_to_P=108.0
        self.C_P_to_GAs=33.0
        self.C_GAs_to_P=33.0
        self.dt = 1./2048.
        self.NbODEs = 10
        self.init_vector( )

    def init_vector(self):
        self.dydt = np.zeros(self.NbODEs)
        self.y = np.zeros(self.NbODEs)
        self.yt = np.zeros(self.NbODEs)
        self.dydx1 = np.zeros(self.NbODEs)
        self.dydx2 = np.zeros(self.NbODEs)
        self.dydx3 = np.zeros(self.NbODEs)
        self.LFP = 0.

    def noise_N(self):
        return random.gauss(self.m_N,self.s_N)

    def sigm_P(self,v):
        return  self.e0_P/(1+np.exp(self.r_P*(self.v0_P-v)))

    def sigm_Pp(self,v):
        return  self.e0_Pp/(1+np.exp(self.r_Pp*(self.v0_Pp-v)))

    def sigm_GAs(self,v):
        return  self.e0_GAs/(1+np.exp(self.r_GAs*(self.v0_GAs-v)))

    def PTW(self,y0,y1,y2,V,v):
        return (V*v*y0 - 2*v*y2 - v*v*y1)

    def derivT(self):
        self. noise_var_N = self.noise_N()
        self.yt = self.y+0.
        self.dydx1=self.deriv()
        self.y = self.yt + self.dydx1 * self.dt / 2
        self.dydx2=self.deriv()
        self.y = self.yt + self.dydx2 * self.dt / 2
        self.dydx3=self.deriv()
        self.y = self.yt + self.dydx3 * self.dt
        self.y =self.yt + self.dt/6. *(self.dydx1+2*self.dydx2+2*self.dydx3+self.deriv())
        self.LFP =  +  self.y[2] -  self.y[6]
        self.PSP_P =  +  self.y[2] -  self.y[6]
        self.FR_P = self.sigm_P(self.PSP_P)
        self.PSP_Pp =  +  self.y[0]
        self.FR_Pp = self.sigm_Pp(self.PSP_Pp)
        self.PSP_GAs =  +  self.y[4]
        self.FR_GAs = self.sigm_GAs(self.PSP_GAs)

    def deriv_Time(self,N):
        lfp = np.zeros(N,)
        for k in range(N):
            self.derivT()
            lfp[k]= self.LFP
        return lfp

    def deriv(self):
        self.dydt[0] = self.y[1]
        self.dydt[1] = self.PTW(+ self.C_P_to_Pp* self.sigm_P( + self.y[8] +  self.y[2] -  self.y[6]), self.y[0],self.y[1],self.H_P , self.T_P)
        self.dydt[2] = self.y[3]
        self.dydt[3] = self.PTW(+ self.C_Pp_to_P* self.sigm_Pp( +  self.y[0]), self.y[2],self.y[3],self.H_Pp , self.T_Pp)
        self.dydt[4] = self.y[5]
        self.dydt[5] = self.PTW(+ self.C_P_to_GAs* self.sigm_P( + self.y[8] +  self.y[2] -  self.y[6]), self.y[4],self.y[5],self.H_P , self.T_P)
        self.dydt[6] = self.y[7]
        self.dydt[7] = self.PTW(+ self.C_GAs_to_P* self.sigm_GAs( +  self.y[4]), self.y[6],self.y[7],self.H_GAs , self.T_GAs)
        self.dydt[8] = self.y[9]
        self.dydt[9] = self.PTW(self.noise_var_N, self.y[8], self.y[9], self.H_P , self.T_P)

        return self.dydt+0.
