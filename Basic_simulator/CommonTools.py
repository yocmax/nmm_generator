# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import numpy as np
import scipy as sc
from scipy import signal


# op on string
def replacecomma(string):
    string= string.replace(',','.')
    string= string.replace(" ",",")
    string= string.replace("  ",",")
    string= string.replace(".",",")
    string= string.replace(";",",")
    string= string.replace(":",",")
    string= string.replace("/",",")
    return string
def convertlistiofstring2listofint(strInh):
    listInh = str(strInh).split(',')
    if (np.array([ (l=='') for l in listInh ])).any():
        listInh.pop(np.where([ (l=='') for l in listInh ])[0])
    return [ int(l) for l in listInh ]

#QT messages
#info message
def msg_cri(s):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(s)
    msg.setWindowTitle(" ")
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()

#question ok cancel
def questionsure(s):
    msg = QMessageBox()
    msg.setIcon(  QMessageBox.Warning )
    strname = s
    msg.setText(strname)
    msg.setStandardButtons(QMessageBox.Ok|QMessageBox.Cancel)
    ok = msg.exec_()
    if ok==QMessageBox.Ok:
        return True
    else:
        return False

#compute recalage
def recalage1D(sig1,sig2,start,end,maxdec):

    # sig2 -= np.median(sig2[start:-end])
    ic = np.correlate(sig1[start:-end],sig2[start:-end],mode='same')
    taille = int(ic.shape[0]/2)
    ic = ic[taille - maxdec : taille + maxdec]
    delay = int(np.where(ic ==ic.max())[0][0]-ic.shape[0]/2 )
    if delay>=1 and delay<=maxdec:
        return np.hstack((np.zeros(delay),sig2[:-delay]))
        #y2 =self.lfp2
    elif delay>=-maxdec and delay<=-1:
        return np.hstack((sig2[-delay:],np.zeros(-delay) ))
    else:
        return sig2

    # sig2 -= np.median(sig2[start:-end])
    # ic = np.correlate(sig1[start:-end],sig2[start:-end],mode='same')
    # delay = int(np.where(ic ==ic.max())[0][0]-ic.shape[0]/2 )
    # y2=np.zeros(sig1.shape[0])
    # if delay>=1 and delay<=maxdec:
    #     y2[delay:]=sig2[:-delay]
    #     #y2 =self.lfp2
    # elif delay>=-maxdec and delay<=-1:
    #     y2[:delay]=sig2[-delay:]
    # else:
    #     y2 =sig2
    # return y2

#question ok cancel
class spinDialog(QDialog):
    NumGridRows = 3
    NumButtons = 4

    def __init__(self,nb = 0):
        super(spinDialog, self).__init__()
        self.createFormGroupBox(nb)

        buttonBox = QDialogButtonBox(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.formGroupBox)
        mainLayout.addWidget(buttonBox)
        self.setLayout(mainLayout)

        self.setWindowTitle("Form Layout - pythonspot.com")

    def createFormGroupBox(self,nb):
        self.formGroupBox = QGroupBox("")
        layout = QFormLayout()
        layout.addRow(QLabel("The file contain " + str(nb) + " NMMs"))
        layout.addRow(QLabel("Please, select the one you want to load:"))
        self.sb = QSpinBox()
        self.sb.setRange(0,nb)
        layout.addRow(self.sb)
        self.formGroupBox.setLayout(layout)

def NMM_number(nb):
    msg = spinDialog(nb)
    ok = msg.exec_()
    if ok==1:
        return msg.sb.value()
    else:
        return None
