.. -*- mode: rst -*-

----------------

**NMM_Generator**  is an innovative method that automatically generates the neural mass model state-space ODE directly from the user-generated graph


Installation
~~~~~~~~~~~~

**Dependencies**

The GUI uses Python 3 with some module dependencies, which are:

-	PyQt5 >= 5.13.1
-	Numpy >=  1.16.3
-	Matplotlib >= 3.0.3
-	Scipy>= 1.2.1

Make sure those modules are installed with the Python >= 3.6.

Executable files
~~~~~~~~~~~~
The user may use the one file executables which do not need any installation.

For Windows 64bit : NMM_Generator.exe and NMM_Basic_Simulator.exe
 
For OSX 64bit : NMM_Generator.app and NMM_Basic_Simulator.app are in the zipped OSXapp.zip file. Download the OSXapp.zip and unzip it into the same folder. 


Documentation
~~~~~~~~~~~~~

A word document (Manual for NMM generator GUI.docx) along with a demo video (Demo_NMM_Generator.mp4) is provided into the Gitlab folder.
Please, refere to them for usage information.

