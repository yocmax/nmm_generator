__author__ = 'Maxime'
# -*- coding: utf-8 -*-
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import os
import sys
import numpy as np
import pickle
import inspect
import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from scipy import signal
import scipy.io
from CommonTools import msg_cri, NMM_number
import time
from numba import jitclass
from numba import boolean, int32, float64,uint8
import datetime
import struct
import csv
import copy

spec = [('a' ,float64)]
@jitclass(spec)
class fornumba():
    def __init__(self):
        self.a = 1.

def butterlowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    if normal_cutoff<= 0:
        normal_cutoff=0.0000001
    elif normal_cutoff >= 1:
            normal_cutoff = 0.9999999999
    b, a = signal.butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butterhighass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    if normal_cutoff<= 0:
        normal_cutoff=0.0000001
    elif normal_cutoff >= 1:
            normal_cutoff = 0.9999999999
    b, a = signal.butter(order, normal_cutoff, btype='high', analog=False)
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butterlowpass(highcut, fs, order=order)
    y = signal.lfilter(b, a, data)
    b, a = butterhighass(lowcut, fs, order=order)
    y = signal.lfilter(b, a, y)
    return y

class LineEdit(QLineEdit):
    KEY = Qt.Key_Return
    def __init__(self, *args, **kwargs):
        QLineEdit.__init__(self, *args, **kwargs)
        QREV = QRegExpValidator(QRegExp("[+-]?\\d*[\\.]?\\d+"))
        QREV.setLocale(QLocale(QLocale.English))
        self.setValidator(QREV)

    def wheelEvent(self, event):
        if QApplication.keyboardModifiers() == Qt.ControlModifier:
            delta = 1 if event.angleDelta().y() > 0 else -1
            val = float(self.text())
            val += delta
            self.setText(str(val))
            event.accept()

class LineEditX2(QLineEdit):
    KEY = Qt.Key_Return
    def __init__(self, *args, **kwargs):
        QLineEdit.__init__(self, *args, **kwargs)
        QREV = QRegExpValidator(QRegExp("[+-]?\\d+"))
        self.setValidator(QREV)


    def wheelEvent(self, event):
        if QApplication.keyboardModifiers() == Qt.ControlModifier:
            delta = 2 if event.angleDelta().y() > 0 else .5
            val = float(self.text())
            val *= delta
            self.setText(str(val))
            event.accept()

class SurfViewer(QMainWindow):
    def __init__(self, parent=None):
        super(SurfViewer, self).__init__()
        self.parent = parent
        # self.lfp = np.random.rand(len(param1),len(param2),100)
        self.lfp = np.zeros(1000, dtype=np.float64 )
        self.t = np.arange(1000)


        self.centralWidget = QWidget()
        self.color = self.centralWidget.palette().color(QPalette.Background)
        self.setCentralWidget(self.centralWidget)
        self.mainHBOX_param_scene = QHBoxLayout()
        self.setmarginandspacing(self.mainHBOX_param_scene)

        self.Param_box = QWidget()
        # self.Param_box.setFixedWidth(350)
        self.layout_Param_box = QVBoxLayout()

        self.updateWidget = None

        # load model file  button
        self.horizontalGroupBox_LoadModel = QWidget()
        # self.horizontalGroupBox_LoadModel.setFixedSize(330,60)
        self.layout_LoadModel = QHBoxLayout()
        self.setmarginandspacing(self.layout_LoadModel)
        self.Button_LoadMod = QPushButton('Load Model File')
        self.Button_LoadMod.setFixedSize(100,30)
        self.Button_SaveNMM = QPushButton('Save NMM Param')
        self.Button_SaveNMM.setFixedSize(100,30)
        self.Button_LoadNMM = QPushButton('Load NMM Param')
        self.Button_LoadNMM.setFixedSize(100,30)

        self.layout_LoadModel.addWidget(self.Button_LoadMod)
        self.layout_LoadModel.addWidget(self.Button_SaveNMM)
        self.layout_LoadModel.addWidget(self.Button_LoadNMM)
        self.horizontalGroupBox_LoadModel.setLayout(self.layout_LoadModel)

        # Open model GUI modifier
        self.horizontalGroupBox_GUIModifier = QWidget()
        # self.horizontalGroupBox_GUIModifier .setFixedSize(330, 60)
        self.layout_GUIModifier = QHBoxLayout()
        self.setmarginandspacing(self.layout_GUIModifier )
        self.Button_GUIModifier = QPushButton('Model GUI')
        self.Button_GUIModifier.setFixedSize(100, 30)
        self.layout_GUIModifier.addWidget(self.Button_GUIModifier)
        self.horizontalGroupBox_GUIModifier.setLayout(self.layout_GUIModifier )


        #actions
        self.horizontalGroupBox_Actions = QWidget()
        # self.horizontalGroupBox_Actions.setFixedSize(330,70)
        self.layout_Actions = QHBoxLayout()
        self.setmarginandspacing(self.layout_Actions)
        self.Button_Run = QPushButton('Run')
        self.Button_Stop = QPushButton('Stop')
        self.Button_LoadRes = QPushButton('Load Result')
        self.Button_SaveRes = QPushButton('Save Result')
        self.Button_Run.setFixedSize(75,30)
        self.Button_Stop.setFixedSize(75,30)
        self.Button_LoadRes.setFixedSize(75,30)
        self.Button_SaveRes.setFixedSize(75,30)
        self.layout_Actions.addWidget(self.Button_Run)
        self.layout_Actions.addWidget(self.Button_Stop)
        self.layout_Actions.addWidget(self.Button_SaveRes)
        self.layout_Actions.addWidget(self.Button_LoadRes)
        self.horizontalGroupBox_Actions.setLayout(self.layout_Actions)

        #filter
        self.horizontalGroupBox_filter_layout = QHBoxLayout()
        self.lfpplot_filtre_Active  = QCheckBox('Active')
        self.lfpplot_filtre_order_l = QLabel("Order")
        self.lfpplot_filtre_order = LineEdit('1')
        self.lfpplot_filtre_FCL_l = QLabel("L(Hz")
        self.lfpplot_filtre_FCL = LineEdit('1')
        self.lfpplot_filtre_FCH_l = QLabel("H(Hz")
        self.lfpplot_filtre_FCH = LineEdit('1000')
        self.lfpplot_Cut_l= QLabel('cut s()')
        self.lfpplot_Cut_Edit= LineEdit('0')

        self.lfpplot_filtre_Active.stateChanged.connect(self.on_lfpPlot_checkboxes_changed)
        self.lfpplot_filtre_order.returnPressed.connect(self.on_lfpPlot_checkboxes_changed)
        self.lfpplot_filtre_FCL.returnPressed.connect(self.on_lfpPlot_checkboxes_changed)
        self.lfpplot_filtre_FCH.returnPressed.connect(self.on_lfpPlot_checkboxes_changed)
        self.lfpplot_Cut_Edit.returnPressed.connect(self.on_lfpPlot_checkboxes_changed)
        self.horizontalGroupBox_filter_layout.addWidget(self.lfpplot_filtre_Active)
        self.horizontalGroupBox_filter_layout.addWidget(self.lfpplot_filtre_order_l)
        self.horizontalGroupBox_filter_layout.addWidget(self.lfpplot_filtre_order)
        self.horizontalGroupBox_filter_layout.addWidget(self.lfpplot_filtre_FCL_l)
        self.horizontalGroupBox_filter_layout.addWidget(self.lfpplot_filtre_FCL)
        self.horizontalGroupBox_filter_layout.addWidget(self.lfpplot_filtre_FCH_l)
        self.horizontalGroupBox_filter_layout.addWidget(self.lfpplot_filtre_FCH)
        self.horizontalGroupBox_filter_layout.addWidget(self.lfpplot_Cut_l)
        self.horizontalGroupBox_filter_layout.addWidget(self.lfpplot_Cut_Edit)
        # console
        self.horizontalGroupBox_Console = QGroupBox("Console")
        self.layout_Consol_Label = QVBoxLayout()
        self.Consol_Label = QTextEdit('Console :')
        self.layout_Consol_Label.addWidget(self.Consol_Label)
        self.horizontalGroupBox_Console.setLayout(self.layout_Consol_Label)

        self.layout_Param_box.addWidget(self.horizontalGroupBox_LoadModel)
        self.layout_Param_box.addWidget(self.horizontalGroupBox_GUIModifier)

        # time
        self.horizontalGroupBox_setParam3 = QGroupBox("Set Time (s)")
        self.Param3_range = QHBoxLayout()
        self.setmarginandspacing(self.Param3_range)
        # self.horizontalGroupBox_setParam3.setFixedSize(330,90)
        grid3 = QGridLayout()
        self.Param3_range.addLayout(grid3)
        self.Param3_min_Label = QLabel('start')
        self.Param3_min_Edit = LineEdit('0')
        self.Param3_max_Label = QLabel('end')
        self.Param3_max_Edit = LineEdit('10')
        self.Param3_step_Label = QLabel('Fs (Hz)')
        self.Param3_step_Edit = LineEditX2('512')
        self.Param3_min_Label.setFixedSize(100,20)
        self.Param3_min_Edit.setFixedSize(100,30)
        self.Param3_max_Label.setFixedSize(100,20)
        self.Param3_max_Edit.setFixedSize(100,30)
        self.Param3_step_Label.setFixedSize(100,20)
        self.Param3_step_Edit.setFixedSize(100,30)
        grid3.addWidget(self.Param3_min_Label, 0, 0)
        grid3.addWidget(self.Param3_min_Edit, 1, 0)
        grid3.addWidget(self.Param3_max_Label, 0, 1)
        grid3.addWidget(self.Param3_max_Edit, 1, 1)
        grid3.addWidget(self.Param3_step_Label, 0, 2)
        grid3.addWidget(self.Param3_step_Edit, 1, 2)
        self.horizontalGroupBox_setParam3.setLayout(self.Param3_range)

        self.layout_Param_box.addWidget(QLabel("Set Parameter:"))
        self.layout_Param_box.addWidget(self.horizontalGroupBox_setParam3)
        self.layout_Param_box.addWidget(self.horizontalGroupBox_Actions)
        self.layout_Param_box.addLayout(self.horizontalGroupBox_filter_layout)

        # Param2 layout
        self.horizontalGroupBox_Param2 = QWidget()
        self.Param2_layout =  QVBoxLayout()
        self.UpdateLayout()

        self.layout_Param_box.addWidget(self.horizontalGroupBox_Console)

        self.plotview = QWidget()
        self.layout_plotview = QVBoxLayout()
        self.mascenelfp = lfpViewer(self)
        self.layout_plotview.addWidget(self.mascenelfp)
        self.plotview.setLayout(self.layout_plotview)

        self.Param_box.setLayout(self.layout_Param_box)
        self.plotview.setLayout(self.layout_plotview)

        # list group view side
        self.splitter2 = QSplitter(Qt.Horizontal)
        self.splitter2.addWidget(self.Param_box)
        self.splitter2.addWidget(self.plotview)
        self.splitter2.setSizes([350,2000] )

        self.mainHBOX_param_scene.addWidget(self.splitter2)
        self.centralWidget.setLayout(self.mainHBOX_param_scene)

        # set actions
        exitAction = QAction(QIcon(os.path.join('icons','exit.png')), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setToolTip('Exit application')
        exitAction.triggered.connect(self.close)

        loadAction = QAction(QIcon(os.path.join('icons','open.png')), 'Load', self)
        loadAction.setShortcut('Ctrl+O')
        loadAction.setToolTip('load Neural Mass Diagram')
        loadAction.triggered.connect(self.load)
        loadAction.setEnabled(False)

        saveAction = QAction(QIcon(os.path.join('icons','save.png')), 'Save', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.setToolTip('load Neural Mass Diagram')
        saveAction.triggered.connect(self.save)
        saveAction.setEnabled(False)
        # Menu
        menubar = self.menuBar()
        menubar.setNativeMenuBar(False)
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)
        fileMenu.addAction(loadAction)
        fileMenu.addAction(saveAction)

        self.Button_Run.clicked.connect(self.Runclick)
        self.Button_LoadMod.clicked.connect(self.load)
        self.Button_SaveNMM.clicked.connect(self.saveNMM)
        self.Button_LoadNMM.clicked.connect(self.loadNMM)
        self.Button_GUIModifier.clicked.connect(self.GUIModifier_fun)
        self.Button_SaveRes.clicked.connect(self.SaveRes)
        self.Button_LoadRes.clicked.connect(self.LoadRes)
        self.Button_SaveRes.setEnabled(True)
        self.Button_LoadRes.setEnabled(False)
        self.Button_Stop.clicked.connect(self.Stopclick)


        self.stop=False

    # def closeEvent(self, event):
    #     print("User has clicked the red x on the main window")
    #     event.accept()

    def GUIModifier_fun(self):
        if not hasattr(self,'monmodel'):
            msg_cri("No Model loaded for identification!" )
            return
        try:
            self.Norm = self.mod.z_Model_GUI(model=self.monmodel)
            self.Norm.setWindowModality(Qt.ApplicationModal)
            self.Norm.Mod_OBJ.connect(self.Apply_z_Model_GUI)
            self.Norm.show()
        except:
            msg_cri("The loaded model has no embedded GUI" )

    @pyqtSlot(list)
    def Apply_z_Model_GUI(self, model):
        self.monmodel = model[0]
        self.UpdateLayout()


    def on_lfpPlot_checkboxes_changed(self):
        self.mascenelfp.updatelfp()

    def UpdateLayout(self):
        if (self.updateWidget == None):
            self.updateWidget = QWidget()
        else:
            self.layout_Param_box.removeWidget(self.updateWidget)
            self.updateWidget.deleteLater()
            self.updateWidget = None

        cnt =  self.mainHBOX_param_scene.layout().count()
        print("Widget count:" +str(cnt)+ "")

        groupscrolllayouttest = QHBoxLayout()
        groupscrolllayouttest.setContentsMargins(0,0,0,0)
        groupscrollbartest = QWidget()

        #self.horizontalGroupBox_Param2.setMaximumHeight(150)
        grid2 = QGridLayout()
        self.Para_Label =  QLabel('Name')
        self.Para_Val = QLabel('Value')
        grid2.addWidget(self.Para_Label, 0, 0)
        grid2.addWidget(self.Para_Val, 0, 1)
        try:
            # self.listvariables=sorted(variablesinclass(self.monmodel))
            if 'dt' in self.listvariables:
                self.listvariables.remove('dt')
            self.listofedit=[]
            for i in np.arange(len(self.listvariables)):
                print(i)
                self.listofedit.append([ QLabel(self.listvariables[i]),
                                         #QLineEdit(str(self.monmodel.__dict__[self.listvariables[i]])),
                                         LineEdit(str(getattr(self.monmodel,self.listvariables[i]))),
                                         QPushButton('+'),
                                         QPushButton('-'),
                                         LineEdit('0.')])
                # self.listofedit[i][0].setMaximumHeight(30)
                # self.listofedit[i][0].setMinimumWidth(50)
                self.listofedit[i][1].setMaximumHeight(30)
                self.listofedit[i][1].setMaximumWidth(60)
                self.listofedit[i][2].setMaximumHeight(30)
                self.listofedit[i][2].setMaximumWidth(30)
                self.listofedit[i][3].setMaximumHeight(30)
                self.listofedit[i][3].setMaximumWidth(30)
                self.listofedit[i][4].setMaximumHeight(30)
                self.listofedit[i][4].setMaximumWidth(60)


                grid2.addWidget(self.listofedit[i][0], i+1, 0)
                grid2.addWidget(self.listofedit[i][1], i+1, 1)
                grid2.addWidget(self.listofedit[i][2], i+1, 2)
                grid2.addWidget(self.listofedit[i][3], i+1, 3)
                grid2.addWidget(self.listofedit[i][4], i+1, 4)

                self.listofedit[i][2].clicked.connect(lambda state,label=self.listofedit[i][0], value=self.listofedit[i][1],delta=self.listofedit[i][4]: self.plus_click(label,value,delta))
                self.listofedit[i][3].clicked.connect(lambda state,label=self.listofedit[i][0], value=self.listofedit[i][1],delta=self.listofedit[i][4]: self.moins_click(label,value,delta))
        except:
            pass

        scroll = QScrollArea()
        scroll.setFrameShape(QFrame.NoFrame)
        widget = QWidget(self)
        widget.setLayout(QHBoxLayout())
        widget.layout().addWidget(groupscrollbartest)
        scroll.setWidget(widget)
        scroll.setWidgetResizable(True)
        groupscrollbartest.setLayout(grid2)
        groupscrolllayouttest.addWidget(scroll)

        self.updateWidget =  QWidget()
        self.updateWidget.setLayout(groupscrolllayouttest)
        self.updateWidget.setContentsMargins(0,0,0,0)
        #self.updateWidget.setMaximumHeight(nbparam*60)
        self.Param2_layout.addWidget(self.updateWidget,)
        self.Param2_layout.setContentsMargins(0,0,0,0)
        self.horizontalGroupBox_Param2.setLayout(self.Param2_layout)
        self.horizontalGroupBox_Param2.setContentsMargins(5,5,5,5)
        #self.mainHBOX_param_scene.addWidget(self.horizontalGroupBox_Param2)
        # NEW
        self.layout_Param_box.insertWidget(3,self.horizontalGroupBox_Param2)

        return

    def updateValue(self):
        for i in range(len(self.listofedit)):
            self.listofedit[i][1].setText(str(getattr(self.monmodel,self.listofedit[i][0].text())) )
        return True

    def plus_click(self,label,value,delta):
        try:
            for p in self.listofedit:
                setattr(self.monmodel, p[0].text(), np.float64(p[1].text().replace(',', '.')))
            setattr(self.monmodel,label.text(),np.float64(value.text().replace(',','.')) + np.float64(delta.text().replace(',','.')))
            self.updateValue()
            self.Runclick()
            return True
        except:
            return False

    def moins_click(self,label,value,delta):
        try:
            for p in self.listofedit:
                setattr(self.monmodel, p[0].text(), np.float64(p[1].text().replace(',', '.')))
            setattr(self.monmodel,label.text(),np.float64(value.text().replace(',','.')) - np.float64(delta.text().replace(',','.')))
            self.updateValue()
            self.Runclick()
            return True
        except:
            return False

    def setmarginandspacing(self, layout):
        layout.setContentsMargins(5,5,5,5)
        layout.setSpacing(5)


    def Stopclick(self):
        self.stop=True



    def SaveRes(self,):
        fileName = QFileDialog.getSaveFileName(caption='Save parameters', filter= ".csv (*.csv);;.data (*.data);;.mat (*.mat);;.bin (*.bin)")
        if  (fileName[0] == '') :
            return

        Sigs_dict = {}
        Sigs_dict["t"] = self.t

        Fs=int(1/(self.t[1]-self.t[0]))
        lfp = copy.copy(self.lfp)
        pulse = copy.copy(self.Pulses)
        pps = copy.copy(self.PPS)
        cut = int(float(self.lfpplot_Cut_Edit.text()) * Fs)

        if self.lfpplot_filtre_Active.isChecked():
            order = int(self.lfpplot_filtre_order.text())
            lowcut = float(self.lfpplot_filtre_FCL.text())
            highcut = float(self.lfpplot_filtre_FCH.text())
            if lowcut <0:
                lowcut=0
            if highcut > Fs/2:
                highcut = Fs/2

            for idx_lfp in range(lfp.shape[1]):
                lfp[:,idx_lfp]=butter_bandpass_filter(lfp[:,idx_lfp], lowcut, highcut, Fs, order)

        tp = self.t[cut:]
        lfp= lfp[cut:,:]
        pulse = pulse[cut:,:]
        pps = pps[cut:,:]

        for i in range(self.lfp.shape[1]):
            try:
                Sigs_dict[self.LFP_Name[i]] = lfp[:,i]
            except:
                Sigs_dict["LFP"+str(i)] = lfp[:,i]

        for i in range(self.Pulses.shape[1]):
            try:
                Sigs_dict[self.Pulses_Names[i]] = pulse[:,i]
            except:
                Sigs_dict["Pulses"+str(i)] = pulse[:,i]

        for i in range(self.PPS.shape[1]):
            try:
                Sigs_dict[self.PPS_Names[i]] = pps[:,i]
            except:
                Sigs_dict["PPS"+str(i)] = pps[:,i]


        if fileName[1] == '.data (*.data)' :
            file_pi = open(fileName[0], 'wb')
            pickle.dump(Sigs_dict, file_pi, -1)
            file_pi.close()
        elif fileName[1] == '.bin (*.bin)':
            #write .des file
            path, name = os.path.split(fileName[0])
            name = name.split('.')[0]
            file = open(os.path.join(path,name+'.des'),"w")
            file.write("[patient]  x" + '\n')
            file.write("[date] "+datetime.datetime.today().strftime('%m/%d/%Y')+ '\n')
            file.write("[time] "+datetime.datetime.today().strftime('%H:%M:%S')+ '\n')
            Fs = int(1./(self.t[1]-self.t[0]))
            file.write("[samplingfreq] " +str(Fs)+ '\n')
            file.write("[nbsegments] 1"+ '\n')
            file.write("[enabled] 1"+ '\n')
            nsample = len(self.t)
            file.write("[nbsamples] " + str(nsample)+ '\n')
            file.write("[segmentsize] " + str(nsample)+ '\n')
            file.write("[segmentInitialTimes] 0.0"+ '\n')
            file.write("[nbchannels] "+ str(len(Sigs_dict))+ '\n')
            file.write("[channelnames] :"+ '\n')
            for s in Sigs_dict.keys():
                file.write(s+" ------"+ '\n')
            # file.write('aaa'+" ------"+ '\n')
            file.close()
            keys = list(Sigs_dict.keys())
            array = np.array(Sigs_dict[keys[0]])
            for s in keys[1:]:
                array = np.vstack((array, Sigs_dict[s]))
            array = array.T.flatten()
            array.astype('float32')
            s = struct.pack('f'*len(array), *array)
            file = open(os.path.join(path,name+'.bin'),"wb")
            # array.tofile(file)
            file.write(s)
            file.close()

        elif fileName[1] == '.mat (*.mat)':
            scipy.io.savemat(fileName[0], mdict=Sigs_dict)
        elif fileName[1] == '.csv (*.csv)':
            f = open(fileName[0],'w')
            w = csv.writer(f , delimiter='\t', lineterminator='\n')
            w.writerow(Sigs_dict.keys())
            for values in Sigs_dict.values():
                w.writerow(['{:e}'.format(var) for var in values])
            f.close()

        return

    def LoadRes(self,):
        fileName = QFileDialog.getOpenFileName(self,'Load Data' )
        if  (fileName[0] == '') :
            return
        filehandler = open(fileName[0], 'rb')
        self.xv = pickle.load(filehandler)
        self.yv = pickle.load(filehandler)
        self.t = pickle.load(filehandler)
        self.lfp = pickle.load(filehandler)
        self.Param1str = pickle.load(filehandler)
        self.Param2str = pickle.load(filehandler)
        self.monmodel = pickle.load(filehandler)
        filehandler.close()
        self.Param1_min_Edit.setText(str(self.yv[0,0]))
        self.Param1_max_Edit.setText(str(self.yv[-1,0]))
        self.Param1_step_Edit.setText(str((self.yv[1,0]-self.yv[0,0])))
        self.Param2_min_Edit.setText(str(self.xv[0,0]))
        self.Param2_max_Edit.setText(str(self.xv[0,-1]))
        self.Param2_step_Edit.setText(str(self.xv[0,1]-self.xv[0,0]))
        self.Param3_min_Edit.setText(str(self.t[0]))
        self.Param3_max_Edit.setText(str(self.t[-1]))
        self.Param3_step_Edit.setText(str(1/(self.t[1]-self.t[0])))
        try:
            self.listvariables = self.monmodel.get_Variable_Names()
        except:
            self.listvariables=[]
        if self.listvariables==[]:
            self.listvariables=sorted(variablesinclass(self.monmodel))
        self.Param2.clear()
        self.Param2.addItems(self.listvariables)
        index = self.Param2.findText(self.Param2str, Qt.MatchExactly|Qt.MatchCaseSensitive)
        if index >= 0:
             self.Param2.setCurrentIndex(index)
        self.Param1.clear()
        self.Param1.addItems(self.listvariables)
        index = self.Param1.findText(self.Param1str, Qt.MatchExactly|Qt.MatchCaseSensitive)
        if index >= 0:
             self.Param1.setCurrentIndex(index)
        self.Constante.clear()
        self.Constante.addItems(self.listvariables)
        md = variablesinclass(self.monmodel)
        self.Consol_Label.setText('Model''s constants:\n\n')
        self.Consol_Label.append(str(self.monmodel) +'\n\n')
        for variable in sorted(list(md )):
            if (isinstance(getattr(self.monmodel,variable), float)) or (isinstance(getattr(self.monmodel,variable), int)):
                self.Consol_Label.append(variable + ' = ' + str(getattr(self.monmodel,variable)))
        self.Replot()
        # self.mascenegrid.updategrid()
        return

    def Runclick(self):
        # try:
        if not hasattr(self,'monmodel'):
            msg_cri("No Model loaded for identification!" )
            return
        self.stop = False

        for p in self.listofedit :
            setattr(self.monmodel,p[0].text(),np.float64(p[1].text().replace(',','.')))


        parametret_val = [np.float64(self.Param3_min_Edit.text().replace(',','.')), np.float64(self.Param3_max_Edit.text().replace(',','.')), np.float64(self.Param3_step_Edit.text().replace(',','.'))]
        self.t = np.arange(parametret_val[0],parametret_val[1]+1./parametret_val[2],1./parametret_val[2])
        self.lfp = np.zeros((self.t.shape[0],len(self.LFP_Name)), dtype=np.float64)
        # self.Pulses_Names = self.monmodel.get_Pulse_Names()
        self.Pulses = np.zeros((self.t.shape[0],len(self.Pulses_Names)), dtype=np.float64)
        # self.PPS_Names = self.monmodel.get_PPS_Names()
        self.PPS = np.zeros((self.t.shape[0],len(self.PPS_Names)), dtype=np.float64)

        self.detail = 0
        try:
            setattr(self.monmodel,'dt',np.float64(1./parametret_val[2]))
            self.monmodel.init_vector()
        except:
            pass
        to = time.time()
        for k,t in enumerate(self.t[0:-1]):
            # self.monmodel.derivT()
            self.ODE_solver()
            for idx, s in enumerate(self.LFP_Name):
                self.lfp[k,idx]=getattr(self.monmodel,s)
            for idx, s in enumerate(self.Pulses_Names):
                self.Pulses[k,idx] = getattr(self.monmodel,s)
            for idx, s in enumerate(self.PPS_Names):
                self.PPS[k,idx] = getattr(self.monmodel,s)
        print(time.time() -to )
        self.mascenelfp.updatelfp()
        # except:
        #         pass

        return


    def load(self):
        fileName = QFileDialog.getOpenFileName(self,"Open Data File" , "", "data files (*.py)")
        if fileName[0]=='':
            return
        fileName = str(fileName[0])
        #print(fileName)
        # fileName=fileName.replace('/','\\')
        (filepath, filename) = os.path.split(fileName)
        sys.path.append(filepath)
        (shortname, extension) = os.path.splitext(filename)
        self.mod = __import__(shortname)
        listclass = sorted(classesinmodule(self.mod))
        #print(listclass)
        item, ok = QInputDialog.getItem(self, "Class Model selection","Select a Model Class", listclass, 0, False)
        if ok==False:
            return
        self.item = str(item )
        self.my_class = getattr(self.mod, str(item ))
        self.monmodel = self.my_class()
        self.LFP_Name = self.mod.get_LFP_Name()
        try:
            self.LFP_color = self.mod.get_LFP_color()
        except:
            self.LFP_color =['b']
        self.Pulses_Names = self.mod.get_Pulse_Names()
        self.PPS_Names = self.mod.get_PPS_Names()
        try:
            self.sig_color = self.mod.get_Colors()
        except:
            self.sig_color =['b']*len(self.PPS_Names)

        self.ODE_solver = getattr(self.monmodel, self.mod.get_ODE_solver()[0])

        try:
            self.listvariables = self.mod.get_Variable_Names()
        except:
            self.listvariables=[]
        if self.listvariables==[]:
            self.listvariables=sorted(variablesinclass(self.monmodel))
        self.UpdateLayout()
        return

    def save(self):
        fileName = QFileDialog.getSaveFileName(self,'Save Data')
        if  (fileName[0] == '') :
            return
        fileName = str(fileName[0])
        #print(fileName)
        file_pi = open(fileName + '.obj', 'w')
        pickle.dump(self.Graph_Items, file_pi) #what's append when click on load button?
        file_pi.close()
        #print(self.Graph_Items)
        return

    def saveNMM(self):
        if not hasattr(self,'monmodel'):
            msg_cri("No Model loaded for identification!" )
            return
        listVar = []
        listVal = []
        for p in self.listofedit :
            Var = p[0].text()
            listVar.append(Var)
            Val =np.float64(p[1].text().replace(',','.'))
            listVal.append(Val)
            setattr(self.monmodel,Var,Val)
        self.Save_Model(str(self.mod )  + '<class '+self.item + '>' ,listVar,listVal)

        return
    def Save_Model(self,name=None,listVar=None,listVal=None):
        extension = "txt"
        fileName = QFileDialog.getSaveFileName(caption='Save parameters', filter= extension +" (*." + extension +")")
        if  (fileName[0] == '') :
            return
        if os.path.splitext(fileName[0])[1] == '':
            fileName= (fileName[0] + '.' + extension , fileName[1])
        if fileName[1] == extension +" (*." + extension +")" :
            f = open(fileName[0] , 'w')
            self.write_model(f,name,listVar,listVal)
            f.close()

    def write_model(self,f,name,listVar,listVal):
        f.write("Model_info::\n")
        f.write("Model_Name = " + name +"\n")
        f.write("Nb_NMM = " + str(1) + "\n")
        for idx_n, n in enumerate(listVar):
            f.write(n+" ")
            f.write(str(listVal[idx_n])+"\t")
            f.write("\n")

    def loadNMM(self):
        model,modelname = self.Load_Model()
        if model == None:
            msg_cri("Unable to load model")
            return
        knownkey =[]
        unknownkey =[]
        for key in model:
            try:
                getattr(self.monmodel,key)
                knownkey.append(key)
            except:
                unknownkey.append(key)
        if unknownkey:
            quit_msg = "The current NMM does not match the file\n" \
                       "unknown variables: "+ ','.join([str(u) for u in unknownkey]) +"\n" \
                       "Do you want to load only the known parameters?"+"\n"\
                        "known variables: " + ','.join([str(u) for u in knownkey]) +"\n"
            reply =  QMessageBox.question(self, 'Message',
                    quit_msg, QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.No:
                return
        for key, value in model.items():
            try:
                setattr(self.monmodel,key,value)
            except:
                pass
        self.updateValue()



    def Load_Model(self):
        extension = "txt"
        fileName = QFileDialog.getOpenFileName(caption='Load parameters' ,filter= extension +" (*." + extension +")")
        if  (fileName[0] == '') :
            return None,None
        if os.path.splitext(fileName[0])[1] == '':
            fileName= (fileName[0] + '.' + extension , fileName[1])
        if fileName[1] == extension +" (*." + extension +")" :
            f = open(fileName[0] , 'r')
            line =f.readline()
            model=None
            modelname=None
            while not("Model_info::" in line or line == ''):
                line =f.readline()
            if  "Model_info" in line:
                model,modelname,line=self.read_model(f )
            f.close()
            return model,modelname

    def read_model(self,f):
        line =f.readline()
        if '=' in line:
            modelname = line.split('=')[-1]
            line =f.readline()
        else:
            modelname=''
        if '=' in line:
            nbmodel = int(line.split('=')[-1])
            line =f.readline()
        else:
            nbmodel=1

        numero = 0
        if nbmodel > 1:
            numero = NMM_number(nbmodel)

        model={}

        while not("::" in line or line == ''):
            if not (line == '' or line == "\n"):
                lsplit = line.split("\t")
                name = lsplit[0]
                try:
                    val = float(lsplit[numero + 1])
                except:
                    val = lsplit[numero + 1]
                model[name] = val
            line =f.readline()
        return model,modelname,line

class lfpViewer(QGraphicsView):
    def __init__(self, parent=None):
        super(lfpViewer, self).__init__(parent)
        self.parent=parent
        self.setStyleSheet("border: 0px")
        self.scene = QGraphicsScene(self)
        self.setScene(self.scene)
        self.setBackgroundBrush(QBrush(self.parent.color))
        self.figure = Figure(facecolor=[self.parent.color.red()/255,self.parent.color.green()/255,self.parent.color.blue()/255])
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)

        self.save_button = QPushButton()
        self.save_button.setIcon(QIcon(os.path.join('icons','SaveData.png')))
        self.save_button.setToolTip("Save Figure Data")
        self.toolbar.addWidget(self.save_button)
        self.save_button.clicked.connect(self.saveFigData)
        self.save_button.setEnabled(False)
        self.load_button = QPushButton()
        self.load_button.setIcon(QIcon(os.path.join('icons','LoadData.png')))
        self.load_button.setToolTip("Load Figure Data")
        self.toolbar.addWidget(self.load_button)
        self.load_button.clicked.connect(self.loaddatapickle)
        self.load_button.setEnabled(False)
        self.load_scale = QPushButton()
        self.load_scale.setIcon(QIcon(os.path.join('icons','Resize-icon.png')))
        self.load_scale.setToolTip("rescale plot")
        self.toolbar.addWidget(self.load_scale)
        self.load_scale.clicked.connect(self.rescale)

        gs = matplotlib.gridspec.GridSpec(4, 3)
        self.axes = self.figure.add_subplot(gs[0,:])
        self.axes.set_xlabel("Time (s)")
        self.axes.set_ylabel("lfp (mV)")
        self.axes.plot(self.parent.t,self.parent.lfp )
        self.axesPulses = self.figure.add_subplot(gs[1,:],sharex=self.axes)
        self.axesPulses.set_xlabel("Time (s)")
        self.axesPulses.set_ylabel("Pulses (Hz)")
        self.axesPulses.plot(self.parent.t,self.parent.lfp )

        self.axesPPS = self.figure.add_subplot(gs[2,:],sharex=self.axes)
        self.axesPPS.set_xlabel("Time (s)")
        self.axesPPS.set_ylabel("PPS (mV)")
        self.axesPPS.plot(self.parent.t,self.parent.lfp )

        self.axes2 = self.figure.add_subplot(gs[3,0:3])
        self.axes2.set_xlabel("Frequence (Hz)")
        self.axes2.set_ylabel("PSD")
        f, Pxx_den = signal.periodogram(self.parent.lfp , 1./(self.parent.t[1]-self.parent.t[0]))
        self.axes2.plot(f[:np.where(f>0)[0][0]],Pxx_den[:np.where(f>0)[0][0]])
        # self.axesExI = self.figure.add_subplot(gs[3,-1])
        # self.axesExI.set_xlabel("Exc")
        # self.axesExI.set_ylabel("Inh")

        # self.canvas = FigureCanvas(self.figure)
        self.canvas.setGeometry(0, 0, 1500, 500)
        layout = QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        self.setLayout(layout)
        # self.scene.addWidget(self.canvas)

    def rescale(self):
        rescale(self.figure)
        self.canvas.draw_idle()
        self.canvas.show()

    def loaddatapickle(self):
        fileName = QFileDialog.getOpenFileName(self,'Load Figure', '',"Figure (*.figp)")
        if  (fileName[0] == '') :
            return
        fileName = str(fileName[0])
        extension = fileName.split('.')[-1]
        if extension =='figp':
            filehandler = open(fileName , 'rb')
            self.figure = pickle.load(filehandler )
            filehandler.close()
            self.layout().removeWidget(self.canvas)
            self.canvas = FigureCanvas(self.figure)
            self.layout().addWidget(self.canvas, 1)
            self.canvas.draw_idle()

        return

    def saveFigData(self):
        fileName = QFileDialog.getSaveFileName(self,'Save Figure Data', '', "Matlab (*.mat);;python Numpy (*.npz);;Figure (*.figp)")
        if  (fileName[0] == '') :
            return
        fileName = str(fileName[0])
        extension = fileName.split('.')[-1]
        if extension == 'mat':
            scipy.io.savemat(fileName, mdict={'lfp': self.parent.lfp,
                                              't':self.parent.t,
                                              'Yexc':self.parent.Exc,
                                              'Yinh': self.parent.Inh})
        elif extension == 'npz':
            np.savez(fileName,  lfp=self.parent.lfp, t=self.parent.t)
        elif extension =='figp':
            file_pi = open(fileName, 'wb')
            pickle.dump(self.figure, file_pi, -1)
            file_pi.close()
        return

    def updatelfp(self):
        Fs=int(1/(self.parent.t[1]-self.parent.t[0]))
        lfp = copy.copy(self.parent.lfp)
        pulse = copy.copy(self.parent.Pulses)
        pps = copy.copy(self.parent.PPS)
        cut = int(float(self.parent.lfpplot_Cut_Edit.text()) * Fs)

        if self.parent.lfpplot_filtre_Active.isChecked():
            order = int(self.parent.lfpplot_filtre_order.text())
            lowcut = float(self.parent.lfpplot_filtre_FCL.text())
            highcut = float(self.parent.lfpplot_filtre_FCH.text())
            if lowcut <0:
                lowcut=0
            if highcut > Fs/2:
                highcut = Fs/2

            for idx_lfp in range(lfp.shape[1]):
                lfp[:,idx_lfp]=butter_bandpass_filter(lfp[:,idx_lfp], lowcut, highcut, Fs, order)

        tp = self.parent.t[cut:-2]
        lfp= lfp[cut:-2,:]
        pulse = pulse[cut:-2,:]
        pps = pps[cut:-2,:]


        self.axes.clear()
        self.axes.set_xlabel("Time (s)")
        self.axes.set_ylabel("lfp (mV)")
        for s in range(len(self.parent.LFP_Name)):
            self.axes.plot(tp,lfp[:,s],self.parent.LFP_color[s])
        self.axes.legend(self.parent.LFP_Name)

        self.axesPulses.clear()
        self.axesPulses.set_xlabel("Time (s)")
        self.axesPulses.set_ylabel("Pulses (Hz)")
        for s in range(len(self.parent.Pulses_Names)):
            self.axesPulses.plot(tp,pulse[:,s],self.parent.sig_color[s])
        self.axesPulses.legend(self.parent.Pulses_Names)

        self.axesPPS.clear()
        self.axesPPS.set_xlabel("Time (s)")
        self.axesPPS.set_ylabel("PPS (mV)")
        for s in range(len(self.parent.PPS_Names)):
            self.axesPPS.plot(tp,pps[:,s],self.parent.sig_color[s])
        self.axesPPS.legend(self.parent.PPS_Names)

        self.axes2.clear()
        self.axes2.set_xlabel("Frequence (Hz)")
        self.axes2.set_ylabel("PSD")

        for s in range(len(self.parent.LFP_Name)):
             f, Pxx_den = signal.periodogram(lfp[:,s],Fs, window=('tukey', 10) )
             self.axes2.plot(f[:np.where(f>100)[0][0]],Pxx_den[:np.where(f>100)[0][0]])
        self.axes2.set_xticks(np.arange(0, 100+1, 2))
        self.axes2.grid(True)
        self.canvas.draw_idle()
        # self.hplot.set_ydata(self.parent.lfp[self.parent.coordx,self.parent.coory,:])


def classesinmodule(module):
    md = module.__dict__
    return [c for c in md if (inspect.isclass(md[c]))]

def variablesinclass(classdumodel):
    # md = classdumodel.__dict__
    # return [c for c in md if (isinstance(md[c], float))]

    md = dir(classdumodel)
    md=[m for m in md if not m[0] == '_']
    me = []
    for m in md:
        if isinstance(getattr(classdumodel,m), float):
            me.append(m)
    return me


def rescale(fig):
    try:
        axes = fig.get_axes()
        for axe in axes:
            lines = axe.get_lines()
            minmax = [[min(x.get_data()[0] ),max(x.get_data()[0]),min(x.get_data()[1] ),max(x.get_data()[1])] for x in lines]
            minmax= [min([x[0] for x in minmax] ),max([x[1] for x in minmax] ),min([x[2] for x in minmax] ),max([x[3] for x in minmax] ) ]
            axe.set_xlim([minmax[0],minmax[1]])
            axe.set_ylim([minmax[2],minmax[3]])
    except:
        pass

def app_aboutToQuit(app):
    app.exec_()
    return

def main():
    app = QApplication(sys.argv)
    app.setStyle('Windows')
    #app.aboutToQuit.connect(app_aboutToQuit())
    ex = SurfViewer(app)
    ex.setWindowTitle('NMM Basic Simulator')
    ex.showMaximized()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

