# -*- coding: utf-8 -*-
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import os
import sys
import numpy as np
import pickle
import csv
import copy
import subprocess
import platform


def msg_cri(s):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(s)
    msg.setWindowTitle(" ")
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()


def set_QPushButton_background_color(button=None, color=None):
    if color == None or button == None:
        return
    else:
        button.setAutoFillBackground(True)
        values = "{r}, {g}, {b} ".format(r=color.red(),
                                         g=color.green(),
                                         b=color.blue())
        button.setStyleSheet("QPushButton { background-color: rgb(" + values + "); }")


def label_color_clicked(event, button):
    color = QColor(button.palette().button().color())
    colordial = QColorDialog(color)
    colordial.exec_()
    selectedcolor = colordial.currentColor()
    colordial.close()
    set_QPushButton_background_color(button, selectedcolor)
    pass


def Layout_groupbox_Label_Edit(labelGroup='None', label=['None'], edit=['None'], width=None, height_add=None, height_per_line=None):
    layout = QGroupBox(labelGroup)
    if not width == None:
        layout.setFixedWidth(width)
    layout.setAlignment(Qt.AlignCenter)
    layout_range = QVBoxLayout()
    # layout.setFixedHeight( height_add + height_per_line* len(label))
    grid = QGridLayout()
    layout_range.addLayout(grid)
    Edit_List = []
    for idx in range(len(label)):
        Label = QLabel(label[idx])
        Label.setFixedHeight(height_per_line)
        if edit[idx] == None:
            Edit = QLineEdit('')
            Edit.setEnabled(False)
        elif isinstance(edit[idx], str):
            try:
                float(edit[idx])
                Edit = LineEdit_float(edit[idx])
            except ValueError:
                Edit = QLineEdit(edit[idx])
        elif isinstance(edit[idx], list):
            Edit = QComboBox()
            for i, s in enumerate(edit[idx][1]):
                Edit.addItem(s)
            index = Edit.findText(edit[idx][0], Qt.MatchExactly | Qt.MatchCaseSensitive)
            if index >= 0:
                Edit.setCurrentIndex(index)
        elif type(edit[idx]) == type(QColor()):
            Edit = QPushButton('')
            set_QPushButton_background_color(Edit, QColor(edit[idx]))
            Edit.clicked.connect(lambda state, x=Edit: label_color_clicked(state, x))

        Edit.setFixedHeight(height_per_line)
        grid.addWidget(Label, idx, 0)
        grid.addWidget(Edit, idx, 1)
        Edit_List.append(Edit)
    layout.setLayout(layout_range)
    return layout, Edit_List


class LineEdit_int(QLineEdit):
    KEY = Qt.Key_Return

    def __init__(self, *args, **kwargs):
        QLineEdit.__init__(self, *args, **kwargs)
        QREV = QRegExpValidator(QRegExp("[+-]?\\d+"))
        self.setValidator(QREV)

    def wheelEvent(self, event):
        if QApplication.keyboardModifiers() == Qt.ControlModifier:
            delta = 1 if event.angleDelta().y() > 0 else -1
            val = float(self.text())
            val += delta
            self.setText(str(val))
            event.accept()


class LineEdit_float(QLineEdit):
    KEY = Qt.Key_Return

    def __init__(self, *args, **kwargs):
        QLineEdit.__init__(self, *args, **kwargs)
        # QDV = QDoubleValidator(0, float("inf"), 100000)
        QREV = QRegExpValidator(QRegExp("[+-]?\\d*[\\.,]?\\d+"))
        self.setValidator(QREV)

    def wheelEvent(self, event):
        if QApplication.keyboardModifiers() == Qt.ControlModifier:
            delta = 1 if event.angleDelta().y() > 0 else -1
            val = float(self.text())
            val += delta
            self.setText(str(val))
            event.accept()


class SurfViewer(QMainWindow):
    def __init__(self, parent=None):

        super(SurfViewer, self).__init__()

        if platform.system() == 'Darwin':
            self.facteurzoom = 1.05
        else:
            self.facteurzoom = 1.25

        self.colorlist = [QColor(Qt.red), QColor(Qt.blue), QColor(Qt.darkGreen), QColor(Qt.darkMagenta), QColor(Qt.lightGray), QColor('#FF9900'), QColor('#33DDEE')]
        self.centralWidget = QWidget()
        self.color = self.centralWidget.palette().color(QPalette.Background)
        self.setCentralWidget(self.centralWidget)
        self.mainHBOX_param_scene = QHBoxLayout()
        self.mainHBOX_param_scene.setContentsMargins(0, 0, 0, 0)

        self.Param_box = QWidget()  # QGroupBox(" ")
        self.Param_box.setFixedWidth(400)
        self.layout_Param_box = QVBoxLayout()
        self.layout_Param_box.setContentsMargins(5, 5, 5, 5)

        self.horizontalGroupBox_Neuron_link = QGroupBox("Neuron / Link")
        self.layout_Neuron_link = QVBoxLayout()
        grid_general = QGridLayout()
        self.layout_Neuron_link.addLayout(grid_general)
        self.number_group = QButtonGroup(self.layout_Neuron_link)
        self.r2 = QRadioButton("Principal cells")
        self.number_group.addButton(self.r2, -2)
        self.r3 = QRadioButton("GabaA slow")
        self.number_group.addButton(self.r3, -3)
        self.r4 = QRadioButton("GabaA fast")
        self.number_group.addButton(self.r4, -4)
        self.r5 = QRadioButton("GabaA B")
        self.number_group.addButton(self.r5, -5)
        self.r8 = QRadioButton("Somatostatine\ncells")
        self.number_group.addButton(self.r8, -8)
        self.r9 = QRadioButton("Basket cells")
        self.number_group.addButton(self.r9, -9)
        self.r10 = QRadioButton("VIP cells")
        self.number_group.addButton(self.r10, -10)
        self.r200 = QRadioButton("Link")
        self.number_group.addButton(self.r200, -200)
        self.r6 = QRadioButton("Excitatory\ninterneurons")
        self.number_group.addButton(self.r6, -6)
        self.r7 = QRadioButton("Inhibitory\ninterneurons")
        self.number_group.addButton(self.r7, -7)
        self.r100 = QRadioButton("Noise")
        self.number_group.addButton(self.r100, -100)
        self.r2.setFixedHeight(20)
        self.r3.setFixedHeight(20)
        self.r4.setFixedHeight(20)
        self.r5.setFixedHeight(20)
        self.r200.setFixedHeight(20)
        # self.r4.setFixedHeight(20)
        self.r100.setFixedHeight(20)
        grid_general.addWidget(self.r2, 0, 0)
        grid_general.addWidget(self.r6, 1, 0)
        grid_general.addWidget(self.r3, 0, 1)
        grid_general.addWidget(self.r4, 1, 1)
        grid_general.addWidget(self.r5, 2, 1)
        grid_general.addWidget(self.r7, 3, 1)
        grid_general.addWidget(self.r8, 0, 2)
        grid_general.addWidget(self.r9, 1, 2)
        grid_general.addWidget(self.r10, 2, 2)
        grid_general.addWidget(self.r100, 5, 0)
        grid_general.addWidget(self.r200, 5, 1)

        self.horizontalGroupBox_Neuron_link.setLayout(self.layout_Neuron_link)

        label = ['Name', '2e0', 'v0', 'r', 'Hx', 'λx', 'Cx', 'Noise Mean', 'Noise std', 'Noise H/λ', 'Color']
        edit = ['P', '5', '6', '0.56', '10', '100', '100', '90', '30', ['', ['']], QColor(Qt.red)]
        widg, Edit = Layout_groupbox_Label_Edit("Parameters", label, edit, width=None, height_add=20, height_per_line=20)
        self.Name_edit = Edit[0]
        self.e0x_edit = Edit[1]
        self.v0x_edit = Edit[2]
        self.rx_edit = Edit[3]
        self.Hx_edit = Edit[4]
        self.Tx_edit = Edit[5]
        self.Cx_edit = Edit[6]
        self.mean_edit = Edit[7]
        self.std_edit = Edit[8]
        self.noise_GTe = Edit[9]
        self.Colorselection = Edit[10]

        self.horizontalGroupBox_Actions = QWidget()
        # self.horizontalGroupBox_Actions.setFixedSize(300,80)
        self.layout_Actions = QHBoxLayout()
        # self.Button_Add=QPushButton('Add')
        # self.Button_Rem=QPushButton('Rem')
        # self.Button_Mod=QPushButton('Mod')
        # self.Button_Gen=QPushButton('Gen')
        # self.Button_Add.setFixedSize(60,30)
        # self.Button_Rem.setFixedSize(60,30)
        # self.Button_Mod.setFixedSize(60,30)
        # self.Button_Gen.setFixedSize(60,30)
        self.Button_Add = QPushButton(QIcon(os.path.join('icons', 'add.png')), '')
        self.Button_Add.setToolTip('Add Item')
        self.Button_Add.setCheckable(True)
        self.Button_Add.setStyleSheet("QPushButton{border: 0px solid;\n}"
                                      "QPushButton:checked { background-color: darkgreen; }\n")
        self.Button_Add.setIconSize(QSize(40, 40))
        self.Button_Rem = QPushButton(QIcon(os.path.join('icons', 'remove.png')), '')
        self.Button_Rem.setToolTip('Remove Item')
        self.Button_Rem.setCheckable(True)
        self.Button_Rem.setStyleSheet("QPushButton{border: 0px solid;\n}"
                                      "QPushButton:checked { background-color: darkgreen; }\n")
        self.Button_Rem.setIconSize(QSize(40, 40))
        self.Button_Mod = QPushButton(QIcon(os.path.join('icons', 'edit.png')), '')
        self.Button_Mod.setToolTip('Modify Item')
        self.Button_Mod.setCheckable(True)
        self.Button_Mod.setStyleSheet("QPushButton{border: 0px solid;\n}"
                                      "QPushButton:checked { background-color: darkgreen; }\n")
        self.Button_Mod.setIconSize(QSize(40, 40))
        self.Button_Gen = QPushButton(QIcon(os.path.join('icons', 'process.png')), '')
        self.Button_Gen.setToolTip('Generate NMM')
        self.Button_Gen.setStyleSheet('QPushButton{border: 0px solid;}')
        self.Button_Gen.setIconSize(QSize(40, 40))
        self.Button_Add.setFixedSize(40, 40)
        self.Button_Rem.setFixedSize(40, 40)
        self.Button_Mod.setFixedSize(40, 40)
        self.Button_Gen.setFixedSize(40, 40)

        self.layout_Actions.addWidget(self.Button_Add)
        self.layout_Actions.addWidget(self.Button_Rem)
        self.layout_Actions.addWidget(self.Button_Mod)
        self.layout_Actions.addWidget(self.Button_Gen)
        self.horizontalGroupBox_Actions.setLayout(self.layout_Actions)

        self.horizontalGroupBox_ODE_Methods = QGroupBox("ODE Methods")
        self.layout_ODE_Methods = QHBoxLayout()
        self.number_group_ODE_Methods = QButtonGroup(self.layout_ODE_Methods)
        self.oneeqperlink = QRadioButton("1 ODE / link")
        self.number_group_ODE_Methods.addButton(self.oneeqperlink, -2)
        self.oneeqperpop = QRadioButton("1 ODE / pop")
        self.number_group_ODE_Methods.addButton(self.oneeqperpop, -3)
        self.oneeqperlink.setFixedHeight(20)
        self.oneeqperpop.setFixedHeight(20)
        self.layout_ODE_Methods.addWidget(self.oneeqperlink)
        self.layout_ODE_Methods.addWidget(self.oneeqperpop)
        self.horizontalGroupBox_ODE_Methods.setLayout(self.layout_ODE_Methods)
        self.oneeqperlink.setChecked(True)

        self.horizontalGroupBox_ODE_Solvers = QGroupBox("ODE Solvers")
        self.layout_ODE_Solvers = QHBoxLayout()
        self.number_group_ODE_Solvers = QButtonGroup(self.layout_ODE_Solvers)
        self.Euler = QRadioButton("Euler")
        self.number_group_ODE_Solvers.addButton(self.Euler, -2)
        self.Mid_Point = QRadioButton("Midpoint")
        self.number_group_ODE_Solvers.addButton(self.Mid_Point, -3)
        self.RK4 = QRadioButton("Runge-Kutta 4")
        self.number_group_ODE_Solvers.addButton(self.RK4, -4)
        self.Euler.setFixedHeight(20)
        self.Mid_Point.setFixedHeight(20)
        self.RK4.setFixedHeight(20)
        self.layout_ODE_Solvers.addWidget(self.Euler)
        self.layout_ODE_Solvers.addWidget(self.Mid_Point)
        self.layout_ODE_Solvers.addWidget(self.RK4)
        self.horizontalGroupBox_ODE_Solvers.setLayout(self.layout_ODE_Solvers)
        self.RK4.setChecked(True)

        self.horizontalGroupBox_Numba = QGroupBox("Numba optimization")
        self.layout_Numba = QHBoxLayout()
        self.number_group_Numba = QButtonGroup(self.layout_Numba)
        self.withNumba = QRadioButton("With Numba")
        self.number_group_Numba.addButton(self.withNumba, -2)
        self.withoutNumba = QRadioButton("Without Numba")
        self.number_group_Numba.addButton(self.withoutNumba, -3)
        self.withNumba.setFixedHeight(20)
        self.withoutNumba.setFixedHeight(20)
        self.layout_Numba.addWidget(self.withNumba)
        self.layout_Numba.addWidget(self.withoutNumba)
        self.horizontalGroupBox_Numba.setLayout(self.layout_Numba)
        self.withNumba.setChecked(False)

        self.horizontalGroupBox_Console = QGroupBox("Console")
        self.layout_Consol_Label = QVBoxLayout()
        self.Consol_Label = QTextEdit('Console :')
        # self.Consol_Label.setMaximumHeight(100)
        self.layout_Consol_Label.addWidget(self.Consol_Label)
        self.horizontalGroupBox_Console.setLayout(self.layout_Consol_Label)
        self.layout_Param_box.addWidget(self.horizontalGroupBox_Neuron_link)
        self.layout_Param_box.addWidget(self.horizontalGroupBox_ODE_Methods)
        self.layout_Param_box.addWidget(self.horizontalGroupBox_ODE_Solvers)
        # self.layout_Param_box.addWidget(self.horizontalGroupBox_Numba)
        self.layout_Param_box.addWidget(widg)
        self.layout_Param_box.addWidget(self.horizontalGroupBox_Actions)
        self.layout_Param_box.addWidget(self.horizontalGroupBox_Console)

        self.Consol_Label.append('\n start by adding cells')
        # self.Consol_Label.setText('new stuff now')

        self.Param_box.setLayout(self.layout_Param_box)
        self.mainHBOX_param_scene.addWidget(self.Param_box)
        self.mascene = PhotoViewer(self)
        self.mainHBOX_param_scene.addWidget(self.mascene)
        self.centralWidget.setLayout(self.mainHBOX_param_scene)
        # set actions
        exitAction = QAction(QIcon(os.path.join('icons', 'exit.png')), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setToolTip('Exit application')
        exitAction.triggered.connect(self.close)

        newAction = QAction(QIcon(os.path.join('icons', 'new.png')), 'New', self)
        # newAction.setShortcut('Ctrl+n')
        newAction.setToolTip('new model')
        newAction.triggered.connect(self.newsheet)

        loadAction = QAction(QIcon(os.path.join('icons', 'open.png')), 'Load', self)
        loadAction.setShortcut('Ctrl+O')
        loadAction.setToolTip('Load Neural Mass Diagram')
        loadAction.triggered.connect(self.load)

        saveAction = QAction(QIcon(os.path.join('icons', 'save.png')), 'Save', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.setToolTip('Save Neural Mass Diagram')
        saveAction.triggered.connect(self.save)

        undoAction = QAction(QIcon(os.path.join('icons', 'undo-arrow.png')), 'Undo', self)
        undoAction.setShortcut('Ctrl+Z')
        undoAction.setToolTip('Undo')
        undoAction.triggered.connect(self.undo)

        redoAction = QAction(QIcon(os.path.join('icons', 'redo-arrow.png')), 'Redo', self)
        redoAction.setShortcut('Shift+Ctrl+Z')
        redoAction.setToolTip('Redo')
        redoAction.triggered.connect(self.redo)

        openviewer = QAction(QIcon(os.path.join('icons', 'GUI.png')), 'open viewer GUI', self)
        openviewer.setToolTip('open viewer GUI')
        openviewer.triggered.connect(self.openviewer)

        openinfo = QAction(QIcon(os.path.join('icons', 'info.png')), 'open graph info', self)
        openinfo.setToolTip('open graph info')
        openinfo.triggered.connect(self.openinfo)

        resize = QAction(QIcon(os.path.join('icons', 'resize.png')), 'resize view', self)
        resize.setToolTip('resize')
        resize.triggered.connect(self.resize_func)
        # Menu
        # menubar = self.menuBar()
        # menubar.setNativeMenuBar(False)
        # fileMenu = menubar.addMenu('&File')
        # fileMenu.addAction(exitAction)
        # fileMenu.addAction(newAction)
        # fileMenu.addAction(loadAction)
        # fileMenu.addAction(saveAction)

        # toolbar
        toolbar = self.addToolBar('Exit')
        toolbar.addAction(exitAction)
        toolbar.addSeparator()
        toolbar.addAction(newAction)
        toolbar.addAction(loadAction)
        toolbar.addAction(saveAction)
        toolbar.addSeparator()
        toolbar.addAction(undoAction)
        toolbar.addAction(redoAction)
        toolbar.addSeparator()
        toolbar.addAction(openviewer)
        toolbar.addSeparator()
        toolbar.addAction(openinfo)
        toolbar.addSeparator()
        toolbar.addAction(resize)

        self.r2.toggled.connect(self.radio_clicked)
        self.r3.toggled.connect(self.radio_clicked)
        self.r4.toggled.connect(self.radio_clicked)
        self.r5.toggled.connect(self.radio_clicked)
        self.r6.toggled.connect(self.radio_clicked)
        self.r7.toggled.connect(self.radio_clicked)
        self.r200.toggled.connect(self.radio_clicked)
        self.r100.toggled.connect(self.radio_clicked)
        self.r2.setChecked(True)

        self.Button_Add.toggled.connect(self.Addclick)
        self.Button_Rem.toggled.connect(self.Remclick)
        self.Button_Mod.toggled.connect(self.Modclick)
        self.Button_Gen.clicked.connect(self.Genclick)

        self.Graph_Items = []
        self.Graph_Items_Save = []
        self.Graph_Items_Save_redo = []
        self.clickedonitem = -1
        self.sizeundomax = 200

    def resize_func(self):
        self.mascene.resizecontent()

    def openinfo(self):
        exPopup = InfoTable(self, Graph_Items=self.Graph_Items)
        exPopup.exec_()
        exPopup.deleteLater()

    def openviewer(self):
        if platform.system() == 'Windows':
            subprocess.Popen([sys.executable, os.path.join("Basic_simulator", "NMM_Basic_Simulator.py")], shell=False)
        elif platform.system() == 'Darwin':
            subprocess.Popen([sys.executable, os.path.join("Basic_simulator", "NMM_Basic_Simulator.py")], shell=False)
        elif platform.system() == 'Linux':
            subprocess.Popen([sys.executable, os.path.join("Basic_simulator", "NMM_Basic_Simulator.py")], shell=False)
        # exec(open(os.path.join("Basic_simulator","NMM_Basic_Simulator.py")).read())
        # subprocess.Popen(os.path.join("Basic_simulator","NMM_Basic_Simulator.py"), shell=False)

    def forundo(self):
        if len(self.Graph_Items_Save) == 0:
            self.Graph_Items_Save_redo = []
            self.Graph_Items_Save.append(copy.deepcopy(self.Graph_Items))
        elif not self.Graph_Items_Save[-1] == self.Graph_Items:
            self.Graph_Items_Save_redo = []
            self.Graph_Items_Save.append(copy.deepcopy(self.Graph_Items))
            if len(self.Graph_Items_Save) >= self.sizeundomax:
                self.Graph_Items_Save.pop(0)

    def undo(self):
        if len(self.Graph_Items_Save) > 1:
            self.Graph_Items_Save_redo.append(self.Graph_Items_Save.pop(-1))
            self.Graph_Items = copy.deepcopy(self.Graph_Items_Save[-1])
        self.mascene.UpdateScene()

    def redo(self):
        if len(self.Graph_Items_Save_redo) > 0:
            self.Graph_Items = self.Graph_Items_Save_redo.pop(-1)
            self.Graph_Items_Save.append(copy.deepcopy(self.Graph_Items))
        self.mascene.UpdateScene()

    def setmarginandspacing(self, layout):
        layout.setContentsMargins(5, 5, 5, 5)
        layout.setSpacing(5)

    def closeEvent(self, event):

        quit_msg = "Are you sure you want to exit the program?"
        reply = QMessageBox.question(self, 'Message',
                                     quit_msg, QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def newsheet(self):
        self.Graph_Items = []
        reply = QMessageBox.question(self, '', "Are you sure to reset the model?", QMessageBox.Yes | QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.Graph_Items = []
            self.mascene.UpdateScene()

    def load(self):
        fileName = QFileDialog.getOpenFileName(self, caption='Load Data', filter="txt (*.txt);;nmm (*.nmm)")
        if (fileName[0] == ''):
            return
        if fileName[1] == "nmm (*.nmm)":
            filehandler = open(fileName[0], 'rb')
            self.Graph_Items = pickle.load(filehandler)
            filehandler.close()
            # print(self.Graph_Items)
        elif fileName[1] == "txt (*.txt)":
            with open(fileName[0], mode='r') as csv_file:
                reader = csv.DictReader(csv_file)
                fieldnames = reader.fieldnames
                self.Graph_Items = []
                for row in reader:
                    item = new_item_to_plot()
                    for key in fieldnames:
                        if row[key] == 'None':
                            item.dictionnaire[key] = None
                        elif key == 'ID' or key == 'Type' or key == 'cellId_e' or key == 'cellId_r':
                            item.dictionnaire[key] = int(row[key])
                        elif key == 'eq1' or key == 'eq2' or key == 'lfp' or key == 'noise':
                            item.dictionnaire[key] = row[key]
                        elif '[' in row[key]:
                            s = row[key].replace('[', '').replace(']', '').split(' ')
                            item.dictionnaire[key] = QPointF(float(s[0]), float(s[1]))
                        elif '#' in row[key]:
                            item.dictionnaire[key] = QColor(row[key])
                        else:
                            item.dictionnaire[key] = row[key]
                    self.Graph_Items.append(copy.deepcopy(item))

        self.forundo()

        self.mascene.UpdateScene()
        return

    def save(self):
        fileName = QFileDialog.getSaveFileName(self, caption='Save Data', filter="txt (*.txt);;nmm (*.nmm)")
        if (fileName[0] == ''):
            return

        if fileName[1] == "nmm (*.nmm)":
            file_pi = open(fileName[0], 'wb')
            pickle.dump(self.Graph_Items, file_pi, -1)  # what's append when click on load button?
            file_pi.close()
            # print(self.Graph_Items)
        elif fileName[1] == "txt (*.txt)":
            with open(fileName[0], mode='w', newline='') as csv_file:
                fieldnames = new_item_to_plot().dictionnaire.keys()
                writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                writer.writeheader()

                for item in self.Graph_Items:
                    string = [None] * len(fieldnames)
                    for i, key in enumerate(fieldnames):
                        if item.dictionnaire[key] == None:
                            string[i] = 'None'
                        elif type(item.dictionnaire[key]) == type(int()):
                            string[i] = str(item.dictionnaire[key])
                        elif type(item.dictionnaire[key]) == type(float()):
                            string[i] = str(item.dictionnaire[key])
                        elif type(item.dictionnaire[key]) == type(''):
                            string[i] = item.dictionnaire[key]
                        elif type(item.dictionnaire[key]) == type(QPointF()):
                            string[i] = '[' + str(item.dictionnaire[key].x()) + ' ' + str(item.dictionnaire[key].y()) + ']'  # s.replace('[','').replace(']','').split(' ')
                        elif type(item.dictionnaire[key]) == type(QColor()):
                            string[i] = item.dictionnaire[key].name()
                    print(string)
                    dictionary = dict(zip(fieldnames, string))
                    writer.writerow(dictionary)
        return

    def Addclick(self):
        if self.Button_Rem.isChecked() or self.Button_Mod.isChecked():
            self.Button_Add.setChecked(False)

        elif (self.Button_Add.isChecked() == True):
            print('wait for click on the scene')
            if self.number_group.checkedId() in range(-99, -1):
                self.Consol_Label.setText("choose a position for the cell")
            elif self.number_group.checkedId() in [-200]:
                self.Consol_Label.setText("choose the emitting cell")
            elif self.number_group.checkedId() in [-100]:
                self.Consol_Label.setText("choose a position for the Noise block")
        else:
            pass

    def Genclick(self):

        # test if all pop and noise are connected at least ones
        pop_noise = []
        for idx, item in enumerate(self.Graph_Items):  # delet assiciated link
            type = item.dictionnaire["Type"]
            if type in range(-100, -1):
                pop_noise.append([item.dictionnaire["ID"], type, False])
        for idx, item in enumerate(self.Graph_Items):  # delet assiciated link
            type = item.dictionnaire["Type"]
            if type in [-200]:
                for p in pop_noise:
                    if item.dictionnaire["cellId_e"] == p[0] or item.dictionnaire["cellId_r"] == p[0]:
                        p[2] = True
        for p in pop_noise:
            if p[2] == False:
                idx = findID(self.Graph_Items, ID=p[0])
                qm = QMessageBox
                if p[1] == -100:
                    ans = qm.question(self, '', "Noise " + self.Graph_Items[idx].dictionnaire["Name"] + " is not connected\n would you like to continue?", qm.Yes | qm.No)
                    if ans == qm.No:
                        return
                else:
                    ans = qm.question(self, '', "Population " + self.Graph_Items[idx].dictionnaire["Name"] + " is not connected\n would you like to continue?", qm.Yes | qm.No)
                    if ans == qm.No:
                        return

        # Are noise link with a gain and a time constant?
        for idx2, item2 in enumerate(self.Graph_Items):
            type2 = item2.dictionnaire["Type"]
            if type2 in [-100]:
                if item2.dictionnaire["noisegainT"] == '':
                    exPopup = QuestionRaplaceNoisegainT(self, item=item2, Graph_Items=self.Graph_Items,
                                                        texte="The pop (for gain and Time constant) for the noise : " + item2.dictionnaire["Name"] + " doesn't exist\nPlease select one")

                    if exPopup.exec_() == QDialog.Accepted:
                        if item2.dictionnaire['noisegainT'] == '':
                            msg_cri('a referent population must be selected for each noise.')
                            return
                    else:
                        msg_cri('a referent population must be selected for each noise.')
                        return

        Names = findPopNale(self.Graph_Items)
        # Are noise link with a gain and a time constant? if previous pop was deleted
        for idx2, item2 in enumerate(self.Graph_Items):
            type2 = item2.dictionnaire["Type"]
            if type2 in [-100]:
                if not item2.dictionnaire["noisegainT"] in Names:
                    exPopup = QuestionRaplaceNoisegainT(self, item=item2, Graph_Items=self.Graph_Items,
                                                        texte="The pop (for gain and Time constant) for the noise : " + item2.dictionnaire["Name"] + " doesn't exist anymore\nPlease select one")
                    if exPopup.exec_() == QDialog.Accepted:
                        if item2.dictionnaire['noisegainT'] == '':
                            msg_cri('a referent population must be selected for each noise.')
                            return
                    else:
                        msg_cri('a referent population must be selected for each noise.')
                        return

        for idx2, item2 in enumerate(self.Graph_Items):  # give names to FR and OSO
            type2 = item2.dictionnaire["Type"]
            if type2 in range(-99, -1):
                item2.dictionnaire['pulseName'] = 'FR_' + item2.dictionnaire['Name']
                item2.dictionnaire['ppsName'] = 'PSP_' + item2.dictionnaire['Name']
        ###################
        items = []
        POPs_idx = []
        for idx, item in enumerate(self.Graph_Items):  # Seek all pop name to select one as LFP
            if item.dictionnaire['Type'] in range(-99, -1):
                items.append(item.dictionnaire['Name'])
                POPs_idx.append(idx)

        LFPPOP, ok = QInputDialog.getItem(self, "From which pop the LFP is recorded", "From which pop the LFP is recorded\n" + "list of pops:", items, 0, False)
        Selected_pop = [LFPPOP, POPs_idx[items.index(LFPPOP)]]
        if not ok:
            return

        self.mascene.update()

        self.Consol_Label.setText("Computation of the equation set")

        nbequa = 0
        if self.oneeqperlink.isChecked():  # if one ODE per link is selected
            for idx, item in enumerate(self.Graph_Items):  # numero pour chaque equa diff
                if item.dictionnaire['Type'] in [-200]:
                    if not self.Graph_Items[findID(self.Graph_Items, item.dictionnaire['cellId_e'])].dictionnaire['Type'] == -100:
                        item.dictionnaire['eq1'] = str(nbequa)
                        nbequa += 1
                        item.dictionnaire['eq2'] = str(nbequa)
                        nbequa += 1
                    item.dictionnaire['Cname'] = 'C_' + item.dictionnaire['Name']
                    print(item.dictionnaire['Cname'])

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in [-100]:
                    item.dictionnaire['eq1'] = str(nbequa)
                    nbequa += 1
                    item.dictionnaire['eq2'] = str(nbequa)
                    nbequa += 1
                    item.dictionnaire['meanname'] = 'm_' + item.dictionnaire['Name']
                    item.dictionnaire['stdname'] = 's_' + item.dictionnaire['Name']
                    item.dictionnaire['noisename'] = 'noise_' + item.dictionnaire['Name']
                    item.dictionnaire['noisevarname'] = 'noise_var_' + item.dictionnaire['Name']

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in range(-99, -1):
                    item.dictionnaire['Hname'] = 'H_' + item.dictionnaire['Name']
                    item.dictionnaire['Tname'] = 'T_' + item.dictionnaire['Name']
                    item.dictionnaire['e0name'] = 'e0_' + item.dictionnaire['Name']
                    item.dictionnaire['v0name'] = 'v0_' + item.dictionnaire['Name']
                    item.dictionnaire['rname'] = 'r_' + item.dictionnaire['Name']
                    item.dictionnaire['sigmname'] = 'sigm_' + item.dictionnaire['Name']

            ###################
            for idx, item in enumerate(self.Graph_Items):  # For each pop, find pop connected toward it
                itemid = item.dictionnaire['ID']
                if item.dictionnaire['Type'] in range(-99, -1):
                    malist_pop = []
                    malist_noise = []
                    for idx2, item2 in enumerate(self.Graph_Items):
                        if item2.dictionnaire['Type'] in [-200]:
                            if item2.dictionnaire['cellId_r'] == itemid:
                                if self.Graph_Items[findID(self.Graph_Items, item2.dictionnaire['cellId_e'])].dictionnaire['Type'] == -100:
                                    malist_noise.append(idx2)
                                elif self.Graph_Items[findID(self.Graph_Items, item2.dictionnaire['cellId_e'])].dictionnaire['Type'] in range(-99, -1):
                                    malist_pop.append(idx2)
                    item.dictionnaire['linkId_r'] = malist_pop
                    item.dictionnaire['linknoise_r'] = malist_noise
            for idx, item in enumerate(self.Graph_Items):  # Write down lfp equation for each pop + noises
                if item.dictionnaire['Type'] in range(-99, -1):
                    chaine_lfp = ''
                    for i, link in enumerate(item.dictionnaire['linkId_r']):
                        if self.Graph_Items[link].dictionnaire['Type'] == -200:
                            index_item = findID(self.Graph_Items, self.Graph_Items[link].dictionnaire['cellId_e'])
                            item2 = self.Graph_Items[index_item]
                            if not item2.dictionnaire['Type'] == -100:
                                if item2.dictionnaire['Type'] in [-2, -6]:
                                    chaine_lfp = chaine_lfp + ' + '
                                elif item2.dictionnaire['Type'] in [-3, -4, -5, -7, -8, -9, -10]:
                                    chaine_lfp = chaine_lfp + ' - '
                                # chaine_lfp= chaine_lfp + 'self.' +self.Graph_Items[link].dictionnaire['Cname'] + ' * self.y[' + self.Graph_Items[link].dictionnaire['eq1']+']' # C apres H
                                chaine_lfp = chaine_lfp + ' self.y[' + self.Graph_Items[link].dictionnaire['eq1'] + ']'  # C avant H
                            elif item2.dictionnaire['Type'] == -100:
                                chaine_lfp = chaine_lfp + ' + self.y[' + item2.dictionnaire['eq1'] + ']'

                    item.dictionnaire['lfp'] = chaine_lfp

                    chaine_noise = ''
                    for i, link in enumerate(item.dictionnaire['linknoise_r']):
                        if self.Graph_Items[link].dictionnaire['Type'] == -200:
                            index_item = findID(self.Graph_Items, self.Graph_Items[link].dictionnaire['cellId_e'])
                            item2 = self.Graph_Items[index_item]
                            if item2.dictionnaire['Type'] in [-100]:
                                chaine_noise = chaine_noise + ' + self.y[' + item2.dictionnaire['eq1'] + ']'
                    item.dictionnaire['noise'] = chaine_noise

            fileName = QFileDialog.getSaveFileName(self, 'ModelName', QDir.currentPath(), "py Files (*.py)")  # ask for a file name and path
            if (fileName[0] == ''):
                return
            fileName = str(fileName[0])
            f = open(fileName, 'w')
            f.write('import random\n')
            f.write('import numpy as np\n\n')

            if self.withNumba.isChecked():
                f.write('from numba import jitclass\n')
                f.write('from numba import int32, float64\n\n')

            f.write('def get_LFP_Name():\n')
            f.write('    return ["LFP"]\n\n')
            f.write('def get_LFP_color():\n')
            f.write('    return ["' + self.Graph_Items[Selected_pop[1]].dictionnaire['color'].name() + '"]\n\n')
            f.write('def get_Pulse_Names():\n')
            chaine = ""
            for idx2, item2 in enumerate(self.Graph_Items):  # delet assiciated link
                type2 = item2.dictionnaire["Type"]
                if type2 in range(-99, -1):
                    chaine += "\'" + item2.dictionnaire["pulseName"] + "\',"

            f.write('    return [' + chaine + ']\n\n')

            f.write('def get_PPS_Names():\n')
            chaine = ""
            for idx2, item2 in enumerate(self.Graph_Items):  # delet assiciated link
                type2 = item2.dictionnaire["Type"]
                if type2 in range(-99, -1):
                    chaine += "\'" + item2.dictionnaire["ppsName"] + "\',"
            f.write('    return [' + chaine + ']\n\n')

            f.write('def get_Colors():\n')
            chaine = ""
            for idx2, item2 in enumerate(self.Graph_Items):  # delet assiciated link
                type2 = item2.dictionnaire["Type"]
                if type2 in range(-99, -1):
                    chaine += "\'" + item2.dictionnaire["color"].name() + "\',"

            f.write('    return [' + chaine + ']\n\n')

            f.write('def get_ODE_solver():\n')
            f.write('    return ["derivT"]\n\n')
            f.write('def get_ODE_solver_Time():\n')
            f.write('    return ["deriv_Time"]\n\n')
            f.write('def get_Variable_Names():\n')
            f.write('    return [')

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in range(-99, -1):
                    f.write("'" + item.dictionnaire['Hname'] + "',")
                    f.write("'" + item.dictionnaire['Tname'] + "',")
                    f.write("'" + item.dictionnaire['e0name'] + "',")
                    f.write("'" + item.dictionnaire['v0name'] + "',")
                    f.write("'" + item.dictionnaire['rname'] + "',")

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in [-100]:
                    f.write("'" + item.dictionnaire['meanname'] + "',")
                    f.write("'" + item.dictionnaire['stdname'] + "',")
            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in [-200]:
                    if not self.Graph_Items[findID(self.Graph_Items, item.dictionnaire['cellId_e'])].dictionnaire['Type'] == -100:
                        f.write("'" + item.dictionnaire['Cname'] + "',")
            f.write(']\n\n')

            if self.withNumba.isChecked():
                f.write('spec = [\n')

                for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                    if item.dictionnaire['Type'] in range(-99, -1):
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['v0name'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['e0name'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['rname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['Hname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['Tname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['pulseName'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['ppsName'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')

                for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                    if item.dictionnaire['Type'] in [-100]:
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['meanname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['stdname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['noisevarname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')

                for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                    if item.dictionnaire['Type'] in [-200]:
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['Cname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')

                f.write("        ('NbODEs',int32),\n")
                f.write("        ('dt' ,float64),\n")
                f.write("        ('dydt' ,float64[:]),\n")
                f.write("        ('y' ,float64[:]),\n")
                f.write("        ('yt' ,float64[:]),\n")
                f.write("        ('dydx1' ,float64[:]),\n")
                f.write("        ('dydx2' ,float64[:]),\n")
                f.write("        ('dydx3' ,float64[:]),\n")
                f.write("        ('LFP' ,float64)]\n")
                f.write('\n\n')
                f.write("@jitclass(spec)\n")

            f.write('class Model:\n')
            f.write('    def __init__(self,):\n')
            # sigmoide param
            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in range(-99, -1):
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['v0name'] + '=' + str(float(item.dictionnaire['v0x']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['e0name'] + '=' + str(float(item.dictionnaire['e0x']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['rname'] + '=' + str(float(item.dictionnaire['rx']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['Hname'] + '=' + str(float(item.dictionnaire['Hx']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['Tname'] + '=' + str(float(item.dictionnaire['Tx']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['pulseName'] + '= 0.'
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['ppsName'] + '= 0.'
                    f.write('        ' + chaine + '\n')

            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in [-100]:
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['meanname'] + '=' + str(float(item.dictionnaire['mean']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['stdname'] + '=' + str(float(item.dictionnaire['std']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['noisevarname'] + '=0.'
                    f.write('        ' + chaine + '\n')

            for idx, item in enumerate(self.Graph_Items):  # Connectivities
                if item.dictionnaire['Type'] in [-200]:
                    index_item = findID(self.Graph_Items, item.dictionnaire["cellId_e"])
                    if not self.Graph_Items[index_item].dictionnaire['Type'] == -100:
                        chaine = ''
                        chaine = chaine + 'self.' + item.dictionnaire['Cname'] + '=' + str(float(item.dictionnaire['Cx']))
                        f.write('        ' + chaine + '\n')

            f.write('        self.dt = 1./2048.\n')
            f.write('        self.NbODEs = ' + str(nbequa) + '\n')
            f.write('        self.init_vector( )\n\n')

            f.write('    def init_vector(self):\n')
            f.write('        self.dydt = np.zeros(self.NbODEs)\n')
            f.write('        self.y = np.zeros(self.NbODEs)\n')
            f.write('        self.yt = np.zeros(self.NbODEs)\n')
            f.write('        self.dydx1 = np.zeros(self.NbODEs)\n')
            f.write('        self.dydx2 = np.zeros(self.NbODEs)\n')
            f.write('        self.dydx3 = np.zeros(self.NbODEs)\n')
            f.write('        self.LFP = 0.\n')

            f.write('\n')

            # Noise functions
            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in [-100]:
                    chaine = ''
                    chaine = chaine + 'def ' + item.dictionnaire['noisename'] + '(self):'
                    f.write('    ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'return random.gauss(self.' + item.dictionnaire['meanname'] + ',self.' + item.dictionnaire['stdname'] + ')'
                    f.write('        ' + chaine + '\n\n')
            # Sigmoid functions
            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in range(-99, -1):
                    chaine = ''
                    chaine = chaine + 'def ' + item.dictionnaire['sigmname'] + '(self, v):'
                    f.write('    ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'return  self.' + item.dictionnaire['e0name'] + '/(1+np.exp(self.' + item.dictionnaire['rname'] + '*(self.' + item.dictionnaire['v0name'] + '-v)))'
                    f.write('        ' + chaine + '\n\n')

            # PSP functions
            chaine = 'def PTW(self, y0, y1, y2, G, T):'
            f.write('    ' + chaine + '\n')
            chaine = 'return (G*T*y0 - 2*T*y2 - T*T*y1)'
            f.write('        ' + chaine + '\n\n')

            # RK4 functions
            chaine = 'def derivT(self):'
            f.write('    ' + chaine + '\n')
            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in [-100]:
                    chaine = ''
                    chaine = chaine + 'self. ' + item.dictionnaire['noisevarname'] + ' = self.' + item.dictionnaire['noisename'] + '()'
                    f.write('        ' + chaine + '\n')

            if self.number_group_ODE_Solvers.checkedId() == -4:  # rk4
                f.write('        ' + 'self.yt = self.y+0.' + '\n')
                f.write('        ' + 'self.dydx1=self.deriv()' + '\n')
                f.write('        ' + 'self.y = self.yt + self.dydx1 * self.dt / 2' + '\n')
                f.write('        ' + 'self.dydx2=self.deriv()' + '\n')
                f.write('        ' + 'self.y = self.yt + self.dydx2 * self.dt / 2' + '\n')
                f.write('        ' + 'self.dydx3=self.deriv()' + '\n')
                f.write('        ' + 'self.y = self.yt + self.dydx3 * self.dt' + '\n')
                f.write('        ' + 'self.y =self.yt + self.dt/6. *(self.dydx1+2*self.dydx2+2*self.dydx3+self.deriv())' + '\n')
            elif self.number_group_ODE_Solvers.checkedId() == -3:  # midpoint
                f.write('        ' + 'self.yt = self.y+0.' + '\n')
                f.write('        ' + 'self.dydx1=self.deriv()' + '\n')
                f.write('        ' + 'self.y = self.yt + self.dydx1 * self.dt / 2' + '\n')
                f.write('        ' + 'self.dydx2=self.deriv()' + '\n')
                f.write('        ' + 'self.y = self.yt + self.dydx2 * self.dt' + '\n')
                f.write('        ' + 'self.dydx3=self.deriv()' + '\n')
            elif self.number_group_ODE_Solvers.checkedId() == -2:  # euler
                f.write('        ' + 'self.yt = self.y+0.' + '\n')
                f.write('        ' + 'self.dydx1=self.deriv()' + '\n')
                f.write('        ' + 'self.y += (self.dydx1 * self.dt)\n')

            f.write('        ' + 'self.LFP = ' + self.Graph_Items[Selected_pop[1]].dictionnaire['lfp'] + '\n')

            for idx2, item2 in enumerate(self.Graph_Items):  # delet assiciated link
                type2 = item2.dictionnaire["Type"]
                chaine = ''
                if type2 in range(-99, -1):
                    f.write("        self." + item2.dictionnaire["ppsName"] + " = " + item2.dictionnaire['lfp'] + '\n')
                    f.write("        self." + item2.dictionnaire["pulseName"] + " = self." + item2.dictionnaire['sigmname'] + '(self.' + item2.dictionnaire["ppsName"] + ')' + '\n')

            # write vector solver
            f.write('\n    def deriv_Time(self,N):\n')
            f.write('        lfp = np.zeros(N,)\n')
            f.write('        for k in range(N):\n')
            f.write('            self.derivT()\n')
            f.write('            lfp[k]= self.LFP\n')
            f.write('        return lfp\n\n')

            f.write('    ' + 'def deriv(self):' + '\n')

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in [-200]:
                    index_item = findID(self.Graph_Items, item.dictionnaire["cellId_e"])
                    item2 = self.Graph_Items[index_item]
                    if not (item2.dictionnaire['Type'] in [-100]):
                        f.write('        self.dydt[' + item.dictionnaire['eq1'] + '] = self.y[' + item.dictionnaire['eq2'] + ']\n')
                        f.write('        self.dydt[' + item.dictionnaire['eq2'] + '] = self.PTW(')

                        f.write('+ self.' + item.dictionnaire['Cname'])
                        f.write('* self.' + item2.dictionnaire['sigmname'] + '(')
                        f.write(item2.dictionnaire['noise'])
                        f.write(item2.dictionnaire['lfp'] + '), self.y[' + item.dictionnaire['eq1'] + '],self.y[' + item.dictionnaire['eq2'])
                        f.write('],self.' + item2.dictionnaire['Hname'] + ' , self.' + item2.dictionnaire['Tname'] + ')\n')

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                index_item = findName(self.Graph_Items, Name=item.dictionnaire['noisegainT'])
                if item.dictionnaire['Type'] in [-100]:
                    f.write('        self.dydt[' + item.dictionnaire['eq1'] + '] = self.y[' + item.dictionnaire['eq2'] + ']\n')
                    f.write('        self.dydt[' + item.dictionnaire['eq2'] + '] = self.PTW(')
                    if self.Graph_Items[index_item].dictionnaire['Type'] in [-2]:
                        pass
                    elif self.Graph_Items[index_item].dictionnaire['Type'] in [-3, -4, -5, -7, -8, -9, -10]:
                        f.write('-')
                    f.write('self.' + item.dictionnaire['noisevarname'] + ', self.y[')
                    f.write(item.dictionnaire['eq1'] + '], self.y[' + item.dictionnaire['eq2'])
                    f.write('], self.' + self.Graph_Items[index_item].dictionnaire['Hname'] + ' , self.' + self.Graph_Items[index_item].dictionnaire['Tname'] + ')\n')

            f.write('\n        ' + 'return self.dydt+0.' + '\n')

            f.close()
        if self.oneeqperpop.isChecked():

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in [-200]:
                    item.dictionnaire['Cname'] = 'C_' + item.dictionnaire['Name']

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if (item.dictionnaire['Type'] in range(-99, -1) or item.dictionnaire['Type'] in [-100]):
                    item.dictionnaire['eq1'] = str(nbequa)
                    nbequa += 1
                    item.dictionnaire['eq2'] = str(nbequa)
                    nbequa += 1

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in range(-99, -1):
                    item.dictionnaire['Hname'] = 'H_' + item.dictionnaire['Name']
                    item.dictionnaire['Tname'] = 'T_' + item.dictionnaire['Name']
                    item.dictionnaire['e0name'] = 'e0_' + item.dictionnaire['Name']
                    item.dictionnaire['v0name'] = 'v0_' + item.dictionnaire['Name']
                    item.dictionnaire['rname'] = 'r_' + item.dictionnaire['Name']
                    item.dictionnaire['sigmname'] = 'sigm_' + item.dictionnaire['Name']

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in [-100]:
                    item.dictionnaire['meanname'] = 'm_' + item.dictionnaire['Name']
                    item.dictionnaire['stdname'] = 's_' + item.dictionnaire['Name']
                    item.dictionnaire['noisename'] = 'noise_' + item.dictionnaire['Name']
                    item.dictionnaire['noisevarname'] = 'noise_var_' + item.dictionnaire['Name']
                    item.dictionnaire['Cname'] = 'C_' + item.dictionnaire['Name']
            ###################
            for idx, item in enumerate(self.Graph_Items):  # entrées de chaque cell
                itemid = item.dictionnaire['ID']
                if item.dictionnaire['Type'] in range(-99, -1):
                    malist_pop = []
                    malist_noise = []
                    for idx2, item2 in enumerate(self.Graph_Items):
                        if item2.dictionnaire['Type'] in [-200]:
                            if item2.dictionnaire['cellId_r'] == itemid:
                                if self.Graph_Items[findID(self.Graph_Items, item2.dictionnaire['cellId_e'])].dictionnaire['Type'] == -100:
                                    malist_noise.append(idx2)
                                elif self.Graph_Items[findID(self.Graph_Items, item2.dictionnaire['cellId_e'])].dictionnaire['Type'] in range(-99, -1):
                                    malist_pop.append(idx2)
                    item.dictionnaire['linkId_r'] = malist_pop
                    item.dictionnaire['linknoise_r'] = malist_noise
            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in range(-99, -1):
                    chaine_lfp = ''
                    for i, link in enumerate(item.dictionnaire['linkId_r']):
                        if self.Graph_Items[link].dictionnaire['Type'] == -200:
                            itemindex = findID(self.Graph_Items, self.Graph_Items[link].dictionnaire['cellId_e'])
                            if not self.Graph_Items[itemindex].dictionnaire['Type'] == -100:
                                if self.Graph_Items[itemindex].dictionnaire['Type'] in [-2, -6]:
                                    chaine_lfp = chaine_lfp + ' + '
                                elif self.Graph_Items[itemindex].dictionnaire['Type'] in [-3, -4, -5, -7, -8, -9, -10]:
                                    chaine_lfp = chaine_lfp + ' - '
                                chaine_lfp = chaine_lfp + 'self.' + self.Graph_Items[link].dictionnaire['Cname'] + ' * self.y[' + \
                                             self.Graph_Items[self.Graph_Items[link].dictionnaire['cellId_e']].dictionnaire['eq1'] + ']'
                    item.dictionnaire['lfp'] = chaine_lfp

                    chaine_noise = ''
                    for i, link in enumerate(item.dictionnaire['linknoise_r']):
                        if self.Graph_Items[link].dictionnaire['Type'] == -200:
                            itemindex = findID(self.Graph_Items, self.Graph_Items[link].dictionnaire['cellId_e'])
                            if self.Graph_Items[itemindex].dictionnaire['Type'] in [-100]:
                                # chaine_noise= chaine_noise + ' + self.' +self.Graph_Items[self.Graph_Items[link].dictionnaire['cellId_e']].dictionnaire['noisevarname']
                                chaine_noise = chaine_noise + ' + self.y[' + self.Graph_Items[itemindex].dictionnaire['eq1'] + ']'
                                self.Graph_Items[itemindex].dictionnaire['Hname'] = self.Graph_Items[findID(self.Graph_Items, self.Graph_Items[link].dictionnaire['cellId_r'])].dictionnaire['Hname']
                                self.Graph_Items[itemindex].dictionnaire['Tname'] = self.Graph_Items[findID(self.Graph_Items, self.Graph_Items[link].dictionnaire['cellId_r'])].dictionnaire['Tname']
                    item.dictionnaire['noise'] = chaine_noise

            fileName = QFileDialog.getSaveFileName(self, 'ModelName', QDir.currentPath(), "py Files (*.py)")
            if (fileName[0] == ''):
                return
            fileName = str(fileName[0])
            f = open(fileName, 'w')
            f.write('import random\n')
            f.write('import numpy as np\n\n')
            if self.withNumba.isChecked():
                f.write('from numba import jitclass\n')
                f.write('from numba import int32, float64\n\n')

            f.write('def get_LFP_Name():\n')
            f.write('    return ["LFP"]\n\n')
            f.write('def get_LFP_color():\n')
            f.write('    return ["' + self.Graph_Items[Selected_pop[1]].dictionnaire['color'].name() + '"]\n\n')
            f.write('def get_Pulse_Names():\n')
            chaine = ""
            for idx2, item2 in enumerate(self.Graph_Items):  # delet assiciated link
                type2 = item2.dictionnaire["Type"]
                if type2 in range(-99, -1):
                    chaine += "\'" + item2.dictionnaire["pulseName"] + "\',"

            f.write('    return [' + chaine + ']\n\n')
            f.write('def get_PPS_Names():\n')
            chaine = ""
            for idx2, item2 in enumerate(self.Graph_Items):  # delet assiciated link
                type2 = item2.dictionnaire["Type"]
                if type2 in range(-99, -1):
                    chaine += "\'" + item2.dictionnaire["ppsName"] + "\',"
            f.write('    return [' + chaine + ']\n\n')

            f.write('def get_Colors():\n')
            chaine = ""
            for idx2, item2 in enumerate(self.Graph_Items):  # delet assiciated link
                type2 = item2.dictionnaire["Type"]
                if type2 in range(-99, -1):
                    chaine += "\'" + item2.dictionnaire["color"].name() + "\',"

            f.write('    return [' + chaine + ']\n\n')
            f.write('def get_ODE_solver():\n')
            f.write('    return ["derivT"]\n\n')
            f.write('def get_ODE_solver_Time():\n')
            f.write('    return ["deriv_Time"]\n\n')
            f.write('def get_Variable_Names():\n')
            f.write('    return [')

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in range(-99, -1):
                    f.write("'" + item.dictionnaire['Hname'] + "',")
                    f.write("'" + item.dictionnaire['Tname'] + "',")
                    f.write("'" + item.dictionnaire['e0name'] + "',")
                    f.write("'" + item.dictionnaire['v0name'] + "',")
                    f.write("'" + item.dictionnaire['rname'] + "',")

            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in [-100]:
                    f.write("'" + item.dictionnaire['meanname'] + "',")
                    f.write("'" + item.dictionnaire['stdname'] + "',")
            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in [-200]:
                    if not self.Graph_Items[findID(self.Graph_Items, item.dictionnaire['cellId_e'])].dictionnaire['Type'] == -100:
                        f.write("'" + item.dictionnaire['Cname'] + "',")
            f.write(']\n\n')

            if self.withNumba.isChecked():
                f.write('spec = [\n')

                for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                    if item.dictionnaire['Type'] in range(-99, -1):
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['v0name'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['e0name'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['rname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['Hname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['Tname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['pulseName'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['ppsName'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')

                for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                    if item.dictionnaire['Type'] in [-100]:
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['meanname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['stdname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['noisevarname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')

                for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                    if item.dictionnaire['Type'] in [-200]:
                        chaine = ''
                        chaine = chaine + "('" + item.dictionnaire['Cname'] + "' ,float64),"
                        f.write('        ' + chaine + '\n')

                f.write("        ('NbODEs',int32),\n")
                f.write("        ('dt' ,float64),\n")
                f.write("        ('dydt' ,float64[:]),\n")
                f.write("        ('y' ,float64[:]),\n")
                f.write("        ('yt' ,float64[:]),\n")
                f.write("        ('dydx1' ,float64[:]),\n")
                f.write("        ('dydx2' ,float64[:]),\n")
                f.write("        ('dydx3' ,float64[:]),\n")
                f.write("        ('LFP' ,float64)]\n")
                f.write('\n\n')
                f.write("@jitclass(spec)\n")

            f.write('class Model:\n')
            f.write('    def __init__(self,):\n')
            # sigmoide param
            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in range(-99, -1):
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['v0name'] + '=' + str(float(item.dictionnaire['v0x']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['e0name'] + '=' + str(float(item.dictionnaire['e0x']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['rname'] + '=' + str(float(item.dictionnaire['rx']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['Hname'] + '=' + str(float(item.dictionnaire['Hx']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['Tname'] + '=' + str(float(item.dictionnaire['Tx']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['pulseName'] + '= 0.'
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['ppsName'] + '= 0.'
                    f.write('        ' + chaine + '\n')

            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in [-100]:
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['meanname'] + '=' + str(float(item.dictionnaire['mean']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['stdname'] + '=' + str(float(item.dictionnaire['std']))
                    f.write('        ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['noisevarname'] + '=0.'
                    f.write('        ' + chaine + '\n')

            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in [-200]:
                    itemid = findID(self.Graph_Items, item.dictionnaire["cellId_e"])
                    if not (self.Graph_Items[itemid].dictionnaire['Type'] == -100):
                        chaine = ''
                        chaine = chaine + 'self.' + item.dictionnaire['Cname'] + '=' + str(float(item.dictionnaire['Cx']))
                        f.write('        ' + chaine + '\n')

            f.write('        self.dt = 1./2048.\n')
            f.write('        self.NbODEs = ' + str(nbequa) + '\n')
            f.write('        self.init_vector( )\n\n')

            f.write('    def init_vector(self):\n')
            f.write('        self.dydt = np.zeros(self.NbODEs)\n')
            f.write('        self.y = np.zeros(self.NbODEs)\n')
            f.write('        self.yt = np.zeros(self.NbODEs)\n')
            f.write('        self.dydx1 = np.zeros(self.NbODEs)\n')
            f.write('        self.dydx2 = np.zeros(self.NbODEs)\n')
            f.write('        self.dydx3 = np.zeros(self.NbODEs)\n')
            f.write('        self.LFP = 0.\n\n')

            # Noise functions
            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in [-100]:
                    chaine = ''
                    chaine = chaine + 'def ' + item.dictionnaire['noisename'] + '(self):'
                    f.write('    ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'return random.gauss(self.' + item.dictionnaire['meanname'] + ',self.' + item.dictionnaire['stdname'] + ')'
                    f.write('        ' + chaine + '\n\n')
            # Sigmoid functions
            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in range(-99, -1):
                    chaine = ''
                    chaine = chaine + 'def ' + item.dictionnaire['sigmname'] + '(self,v):'
                    f.write('    ' + chaine + '\n')
                    chaine = ''
                    chaine = chaine + 'return  self.' + item.dictionnaire['e0name'] + '/(1+np.exp(self.' + item.dictionnaire['rname'] + '*(self.' + item.dictionnaire['v0name'] + '-v)))'
                    f.write('        ' + chaine + '\n\n')

            # PSP functions
            chaine = 'def PTW(self,y0,y1,y2,V,v):'
            f.write('    ' + chaine + '\n')
            chaine = 'return (V*v*y0 - 2*v*y2 - v*v*y1)'
            f.write('        ' + chaine + '\n\n')

            # RK4 functions
            chaine = 'def derivT(self):'
            f.write('    ' + chaine + '\n')
            for idx, item in enumerate(self.Graph_Items):  # LFP de chaque cell
                if item.dictionnaire['Type'] in [-100]:
                    chaine = ''
                    chaine = chaine + 'self.' + item.dictionnaire['noisevarname'] + ' = self.' + item.dictionnaire['noisename'] + '()'
                    f.write('        ' + chaine + '\n')

            if self.number_group_ODE_Solvers.checkedId() == -4:  # rk4
                f.write('        ' + 'self.yt = self.y+0.' + '\n')
                f.write('        ' + 'self.dydx1=self.deriv()' + '\n')
                f.write('        ' + 'self.y = self.yt + self.dydx1 * self.dt / 2' + '\n')
                f.write('        ' + 'self.dydx2=self.deriv()' + '\n')
                f.write('        ' + 'self.y = self.yt + self.dydx2 * self.dt / 2' + '\n')
                f.write('        ' + 'self.dydx3=self.deriv()' + '\n')
                f.write('        ' + 'self.y = self.yt + self.dydx3 * self.dt' + '\n')
                f.write('        ' + 'self.y =self.yt + self.dt/6. *(self.dydx1+2*self.dydx2+2*self.dydx3+self.deriv())' + '\n')
            elif self.number_group_ODE_Solvers.checkedId() == -3:  # midpoint
                f.write('        ' + 'self.yt = self.y+0.' + '\n')
                f.write('        ' + 'self.dydx1=self.deriv()' + '\n')
                f.write('        ' + 'self.y = self.yt + self.dydx1 * self.dt / 2' + '\n')
                f.write('        ' + 'self.dydx2=self.deriv()' + '\n')
                f.write('        ' + 'self.y = self.yt + self.dydx2 * self.dt' + '\n')
                f.write('        ' + 'self.dydx3=self.deriv()' + '\n')
            elif self.number_group_ODE_Solvers.checkedId() == -2:  # euler
                f.write('        ' + 'self.yt = self.y+0.' + '\n')
                f.write('        ' + 'self.dydx1=self.deriv()' + '\n')
                f.write('        ' + 'self.y += (self.dydx1 * self.dt)\n')

            f.write('        ' + 'self.LFP = ' + self.Graph_Items[Selected_pop[1]].dictionnaire['lfp'] + '\n')

            for idx2, item2 in enumerate(self.Graph_Items):  # delet assiciated link
                type2 = item2.dictionnaire["Type"]
                chaine = ''
                if type2 in range(-99, -1):
                    f.write("        self." + item2.dictionnaire["ppsName"] + " = " + item2.dictionnaire['lfp'] + '\n')
                    f.write("        self." + item2.dictionnaire["pulseName"] + " = self." + item2.dictionnaire['sigmname'] + '(self.' + item2.dictionnaire["ppsName"] + ')' + '\n')

            # write vector solver
            f.write('\n    def deriv_Time(self,N):\n')
            f.write('        lfp = np.zeros(N,)\n')
            f.write('        for k in range(N):\n')
            f.write('            self.derivT()\n')
            f.write('            lfp[k]= self.LFP\n')
            f.write('        return lfp\n\n')

            f.write('    ' + 'def deriv(self):' + '\n')
            # nbequa=0
            for idx, item in enumerate(self.Graph_Items):  # nom pour chaque equa diff
                if item.dictionnaire['Type'] in range(-99, -1):
                    f.write('        self.dydt[' + item.dictionnaire['eq1'] + '] = self.y[' + item.dictionnaire['eq2'] + ']\n')
                    f.write('        self.dydt[' + item.dictionnaire['eq2'] + '] = self.PTW(')
                    f.write('+ self.' + item.dictionnaire['sigmname'] + '(')
                    f.write(item.dictionnaire['noise'])
                    f.write(item.dictionnaire['lfp'] + '), self.y[' + item.dictionnaire['eq1'] + '],self.y[' + item.dictionnaire['eq2'])

                    f.write('],self.' + item.dictionnaire['Hname'] + ' , self.' + item.dictionnaire['Tname'] + ')\n')
                if item.dictionnaire['Type'] in [-100]:
                    index_item = findName(self.Graph_Items, Name=item.dictionnaire['noisegainT'])
                    f.write('        self.dydt[' + item.dictionnaire['eq1'] + '] = self.y[' + item.dictionnaire['eq2'] + ']\n')
                    f.write('        self.dydt[' + item.dictionnaire['eq2'] + '] = self.PTW(')
                    if self.Graph_Items[index_item].dictionnaire['Type'] in [-2, -6]:
                        pass
                    elif self.Graph_Items[index_item].dictionnaire['Type'] in [-3, -4, -5, -7, -8, -9, -10]:
                        f.write(' - ')
                    f.write('self.' + item.dictionnaire['noisevarname'] + ', self.y[')
                    f.write(item.dictionnaire['eq1'] + '],self.y[' + item.dictionnaire['eq2'])
                    f.write('],self.' + self.Graph_Items[index_item].dictionnaire['Hname'] + ' , self.' + self.Graph_Items[index_item].dictionnaire['Tname'] + ')\n')

            f.write('\n        ' + 'return self.dydt+0.' + '\n')

    def Remclick(self):
        if self.Button_Add.isChecked() or self.Button_Mod.isChecked():
            self.Button_Rem.setChecked(False)
        else:
            self.Consol_Label.setText("choose a cell or a link to Remove")

    def Modclick(self):
        if self.Button_Add.isChecked() or self.Button_Rem.isChecked():
            self.Button_Mod.setChecked(False)
        else:
            self.Consol_Label.setText("choose a cell or a link to move")

    def printItem(self, item):
        self.Consol_Label.setText("Info selected item : \n")
        for key in item.dictionnaire.keys():
            ch = key + ' : ' + str(item.dictionnaire[key])
            self.Consol_Label.append(ch)

    def radio_clicked(self):
        if self.number_group.checkedId() in range(-99, -1):
            print('cell picked up')
            self.Name_edit.setDisabled(0)
            self.e0x_edit.setDisabled(0)
            self.v0x_edit.setDisabled(0)
            self.rx_edit.setDisabled(0)
            self.Hx_edit.setDisabled(0)
            self.Tx_edit.setDisabled(0)
            self.Cx_edit.setDisabled(1)
            self.mean_edit.setDisabled(1)
            self.std_edit.setDisabled(1)
            self.noise_GTe.setDisabled(1)
            if self.number_group.checkedId() == -2:
                self.Name_edit.setText('P')
                self.Hx_edit.setText('5')
                self.Tx_edit.setText('100')
                set_QPushButton_background_color(self.Colorselection, self.colorlist[0])
            elif self.number_group.checkedId() in [-3, -8]:
                self.Name_edit.setText('GAs')
                self.Hx_edit.setText('20')
                self.Tx_edit.setText('30')
                set_QPushButton_background_color(self.Colorselection, self.colorlist[1])
            elif self.number_group.checkedId() in [-4, -9]:
                self.Name_edit.setText('GAf')
                self.Hx_edit.setText('20')
                self.Tx_edit.setText('250')
                set_QPushButton_background_color(self.Colorselection, self.colorlist[2])
            elif self.number_group.checkedId() in [-5, -10]:
                self.Name_edit.setText('GB')
                self.Hx_edit.setText('5')
                self.Tx_edit.setText('4.5')
                set_QPushButton_background_color(self.Colorselection, self.colorlist[3])
            elif self.number_group.checkedId() == -6:
                self.Name_edit.setText('EXC')
                self.Hx_edit.setText('5')
                self.Tx_edit.setText('100')
                set_QPushButton_background_color(self.Colorselection, self.colorlist[5])
            elif self.number_group.checkedId() == -7:
                self.Name_edit.setText('INH')
                self.Hx_edit.setText('20')
                self.Tx_edit.setText('50')
                set_QPushButton_background_color(self.Colorselection, self.colorlist[6])

        elif self.number_group.checkedId() in [-200]:
            print('link picked up')
            self.Name_edit.setDisabled(1)
            self.e0x_edit.setDisabled(1)
            self.v0x_edit.setDisabled(1)
            self.rx_edit.setDisabled(1)
            self.Hx_edit.setDisabled(1)
            self.Tx_edit.setDisabled(1)
            self.Cx_edit.setDisabled(0)
            self.mean_edit.setDisabled(1)
            self.std_edit.setDisabled(1)
            self.noise_GTe.setDisabled(1)
        elif self.number_group.checkedId() in [-100]:
            print('Noise picked up')
            self.Name_edit.setDisabled(0)
            self.Name_edit.setText('N')
            self.e0x_edit.setDisabled(1)
            self.v0x_edit.setDisabled(1)
            self.rx_edit.setDisabled(1)
            self.Hx_edit.setDisabled(1)
            self.Tx_edit.setDisabled(1)
            self.Cx_edit.setDisabled(1)
            self.mean_edit.setDisabled(0)
            self.std_edit.setDisabled(0)
            self.noise_GTe.setDisabled(0)
            self.noise_GTe.clear()
            for i, s in enumerate(findPopNale(self.Graph_Items)):
                self.noise_GTe.addItem(s)
            set_QPushButton_background_color(self.Colorselection, self.colorlist[4])
        return

    def get_ID_Max(self):
        idmax = -1
        for idx, item in enumerate(self.Graph_Items):
            ID = item.dictionnaire['ID']
            if ID > idmax:
                idmax = ID
        return idmax


class PhotoViewer(QGraphicsView):
    def __init__(self, parent=None):
        super(PhotoViewer, self).__init__(parent)
        self.setRenderHint(QPainter.Antialiasing)
        self.parent = parent
        self.withouttext = True
        self.linkattente = False
        self.position1 = QPointF()
        self.cellId_e = -1
        self.cellName_e = ''
        self.clickedonitem = -1
        self.scene = QGraphicsScene(self)
        self.setScene(self.scene)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.setFrameShape(QFrame.NoFrame)
        # self.scene.addRect(QRectF(-2000, -2000, 4000, 4000),QPen(Qt.gray, 10))
        self.setBackgroundBrush(QBrush(Qt.white))  # self.parent.color))
        # self.setBackgroundBrush(Qt.white)
        self.scene.update()
        # for item in self.scene.items():

        # Menu
        # menubar = self.menuBar()
        # menubar.setNativeMenuBar(False)
        # fileMenu = menubar.addMenu('&File')
        # fileMenu.addAction(exitAction)
        # fileMenu.addAction(newAction)
        # fileMenu.addAction(loadAction)
        # fileMenu.addAction(saveAction)

    def resizecontent(self):
        print(self.scene.itemsBoundingRect())
        self.scale(1, 1)
        self.setSceneRect(self.scene.itemsBoundingRect())
        self.fitInView(self.scene.itemsBoundingRect(), Qt.KeepAspectRatio)
        # rect = self.scene.itemsBoundingRect()
        # self.scene.setSceneRect(rect)

    def contextMenuEvent(self, event):
        menu = QMenu()
        Add = menu.addAction('Add Item')
        Modify = menu.addAction('Modify Item')
        Remove = menu.addAction('Remove Item')
        action = menu.exec_(event.globalPos())
        if Modify == action:
            self.mousePressEvent(event, rightclickinfp='Modify')
        elif Remove == action:
            self.mousePressEvent(event, rightclickinfp='Remove')
        elif Add == action:
            self.mousePressEvent(event, rightclickinfp='Add')

    def mouseDoubleClickEvent(self, event):
        p = self.mapToScene(event.pos())
        r = self.sceneRect()
        r.moveCenter(p)
        self.setSceneRect(r)

    def mouseReleaseEvent(self, QMouseEvent):
        if self.clickedonitem > -1:
            self.parent.forundo()
        self.clickedonitem = -1
        self.positionclickedon = None

    def mouseMoveEvent(self, event):
        if self.clickedonitem > -1:
            if bool(event.buttons() & Qt.LeftButton):
                if self.parent.Button_Add.isChecked() == False and self.parent.Button_Rem.isChecked() == False and \
                        self.parent.Button_Mod.isChecked() == False and self.parent.Button_Gen.isEnabled() == True:
                    position = QPointF(event.pos())
                    position = self.mapToScene(position.x(), position.y())
                    type = self.parent.Graph_Items[self.clickedonitem].dictionnaire["Type"]
                    if type in range(-99, -1) or type in [-100]:
                        position_c = self.parent.Graph_Items[self.clickedonitem].dictionnaire["Pos"]
                        for idx2, item2 in enumerate(self.parent.Graph_Items):  # delet assiciated link
                            type2 = item2.dictionnaire["Type"]
                            if type2 in [-200]:
                                if item2.dictionnaire["cellId_e"] == self.parent.Graph_Items[self.clickedonitem].dictionnaire['ID']:
                                    self.parent.Graph_Items[idx2].dictionnaire['Pos'] = position
                                elif item2.dictionnaire["cellId_r"] == self.parent.Graph_Items[self.clickedonitem].dictionnaire['ID']:
                                    self.parent.Graph_Items[idx2].dictionnaire['Pos2'] = position
                        self.parent.Graph_Items[self.clickedonitem].dictionnaire['Pos'] = position
                    if type in [-200]:
                        self.parent.Graph_Items[self.clickedonitem].dictionnaire['Pos_c'] = position
                    self.UpdateScene()
        elif self.clickedonitem == -1 and bool(event.buttons() & Qt.LeftButton):
            try:
                p1 = QPointF(self.positionclickedon)
                p2 = QPointF(self.mapToScene(event.pos()))
                r = self.sceneRect()
                r.translate(p1 - p2)
                self.setSceneRect(r)
            except:
                pass

    def mousePressEvent(self, event, rightclickinfp=None):
        try:
            if not bool(event.buttons() & Qt.LeftButton):
                self.linkattente = False
            if self.linkattente == True:
                if self.parent.Button_Rem.isChecked() == True or \
                        self.parent.Button_Mod.isChecked() == True or \
                        not self.parent.number_group.checkedId() in [-200] or \
                        event.modifiers() == Qt.ShiftModifier or \
                        event.modifiers() == Qt.ControlModifier:
                    self.linkattente = False

        except:
            pass

        if self.parent.Button_Add.isChecked() == True or rightclickinfp == 'Add' or self.linkattente == True:

            if self.parent.number_group.checkedId() in range(-100, -1):
                popNames = getNames(self.parent.Graph_Items)
                if self.parent.Name_edit.text().replace(',', '.') in popNames:
                    while self.parent.Name_edit.text().replace(',', '.') in popNames:
                        text, ok = QInputDialog.getText(self, '', "The  Name '" +
                                                        self.parent.Name_edit.text().replace(',', '.') +
                                                        "' Already exists\n Please, enter another one:")
                        if ok:
                            self.parent.Name_edit.setText(text)
                        if not ok:
                            return

            ID = self.parent.get_ID_Max() + 1

            if self.parent.number_group.checkedId() in range(-99, -1):
                position = QPointF(event.pos())
                position = self.mapToScene(position.x(), position.y())
                self.parent.Graph_Items.append(new_item_to_plot(ID=ID,
                                                                Type=self.parent.number_group.checkedId(),
                                                                Name=self.parent.Name_edit.text().replace(',', '.'),
                                                                e0x=self.parent.e0x_edit.text().replace(',', '.'),
                                                                v0x=self.parent.v0x_edit.text().replace(',', '.'),
                                                                rx=self.parent.rx_edit.text().replace(',', '.'),
                                                                Hx=self.parent.Hx_edit.text().replace(',', '.'),
                                                                Tx=self.parent.Tx_edit.text().replace(',', '.'),
                                                                color=QColor(self.parent.Colorselection.palette().button().color()),
                                                                Pos=position))

                self.UpdateScene()
                self.parent.Button_Add.setChecked(0)
            elif self.parent.number_group.checkedId() in [-100]:
                position = QPointF(event.pos())
                position = self.mapToScene(position.x(), position.y())
                self.parent.Graph_Items.append(new_item_to_plot(ID=ID,
                                                                Type=self.parent.number_group.checkedId(),
                                                                Name=self.parent.Name_edit.text().replace(',', '.'),
                                                                mean=self.parent.mean_edit.text().replace(',', '.'),
                                                                std=self.parent.std_edit.text().replace(',', '.'),
                                                                Pos=position,
                                                                color=QColor(self.parent.Colorselection.palette().button().color()),
                                                                noisegainT=self.parent.noise_GTe.currentText()))

                self.UpdateScene()
                self.parent.Button_Add.setChecked(0)
            elif self.parent.number_group.checkedId() in [-200]:
                if self.linkattente == False:
                    self.linkattente = True
                    self.position1 = QPointF(event.pos())
                    self.position1 = self.mapToScene(self.position1.x(), self.position1.y())
                    self.cellId_e = -1
                    self.cellName_e = ''
                    length = np.Inf
                    for idx, item in enumerate(self.parent.Graph_Items):
                        # print(item)
                        itemID = item.dictionnaire["ID"]
                        type = item.dictionnaire["Type"]
                        nom = item.dictionnaire["Name"]
                        position_c = item.dictionnaire["Pos"]
                        if type in range(-99, -1) or type in [-100]:
                            length_c = ((self.position1.x() - position_c.x()) ** 2 + (self.position1.y() - position_c.y()) ** 2) ** .5
                            if length > length_c:
                                length = length_c
                                self.cellId_e = itemID
                                self.cellName_e = nom
                    if length > 100:
                        self.linkattente = False
                    else:
                        index_item = findID(self.parent.Graph_Items, self.cellId_e)
                        self.position1 = self.parent.Graph_Items[index_item].dictionnaire["Pos"]
                        self.parent.Consol_Label.setText("choose the emitting cell")
                        self.parent.Consol_Label.append("\n You selected the cell : " + self.parent.Graph_Items[index_item].dictionnaire["Name"])
                        self.parent.Consol_Label.append("\n choose the receving cell")
                elif self.linkattente == True:
                    self.linkattente = False
                    position2 = QPointF(event.pos())
                    position2 = self.mapToScene(position2.x(), position2.y())
                    cellId_r = -1
                    cellName_r = ''
                    length = np.Inf
                    for idx, item in enumerate(self.parent.Graph_Items):
                        itemID = item.dictionnaire["ID"]
                        type = item.dictionnaire["Type"]
                        nom = item.dictionnaire["Name"]
                        position_c = item.dictionnaire["Pos"]
                        if type in range(-99, -1) or type in [-100]:
                            length_c = ((position2.x() - position_c.x()) ** 2 + (position2.y() - position_c.y()) ** 2) ** .5
                            if length > length_c:
                                length = length_c
                                cellId_r = itemID
                                cellName_r = nom
                    index_item = findID(self.parent.Graph_Items, cellId_r)
                    position2 = self.parent.Graph_Items[index_item].dictionnaire["Pos"]
                    self.parent.Consol_Label.append("\n You selected the cell : " + self.parent.Graph_Items[index_item].dictionnaire["Name"])

                    if self.parent.Graph_Items[index_item].dictionnaire['Type'] == -100:
                        msg_cri('Noise object cannot receive signal')
                        self.parent.Button_Add.setChecked(0)
                        return

                    if cellId_r == self.cellId_e:
                        Pos_c = QPointF(self.position1.x() + 100, self.position1.y() + 100)
                    else:
                        Pos_c = QPointF((position2.x() - self.position1.x()) * 0.5 + self.position1.x(), (position2.y() - self.position1.y()) * 0.5 + self.position1.y())

                    index_item = findID(self.parent.Graph_Items, self.cellId_e)

                    if self.parent.Graph_Items[index_item].dictionnaire['Type'] == -100:
                        Cx = ''
                    else:
                        Cx = self.parent.Cx_edit.text().replace(',', '.')

                    self.parent.Graph_Items.append(new_item_to_plot(ID=ID,
                                                                    Type=self.parent.number_group.checkedId(), \
                                                                    Name=self.cellName_e + '_to_' + cellName_r, \
                                                                    Pos=self.position1, Pos2=position2, \
                                                                    Pos_c=Pos_c, \
                                                                    cellId_e=self.cellId_e, cellId_r=cellId_r, \
                                                                    Cx=Cx))
                    self.parent.forundo()
                    self.parent.Button_Add.setChecked(0)
                    self.UpdateScene()
            self.clickedonitem = -1


        elif self.parent.Button_Rem.isChecked() == True or event.modifiers() == Qt.ShiftModifier or rightclickinfp == 'Remove':
            # select an item

            position = QPointF(event.pos())
            position = self.mapToScene(position.x(), position.y())
            # print(position)
            findellipse = False
            findellipse_pos = []
            for item in self.scene.items():  # seek for ellipse
                if item.type() == QGraphicsEllipseItem().type():
                    # print(item.contains(position))
                    if item.contains(position):
                        # self.scene.removeItem(item)
                        findellipse = True
                        findellipse_pos = item.boundingRect()
                        # print(findellipse_pos)
                        break
            if findellipse == True:
                todelet = []
                for idx, item in enumerate(self.parent.Graph_Items):
                    type = item.dictionnaire["Type"]
                    if type in range(-99, -1) or type in [-100]:
                        position_c = item.dictionnaire["Pos"]
                        if position_c == findellipse_pos.center():
                            itemid = item.dictionnaire["ID"]
                            Name = item.dictionnaire["Name"]
                            for idx2, item2 in enumerate(self.parent.Graph_Items):  # delet assiciated link
                                type2 = item2.dictionnaire["Type"]
                                if type2 in [-200]:
                                    if item2.dictionnaire["cellId_e"] == itemid:
                                        todelet.append(idx2)
                                    elif item2.dictionnaire["cellId_r"] == itemid:
                                        todelet.append(idx2)

                            todelet.append(idx)
                            for todel in reversed(sorted(todelet)):
                                del self.parent.Graph_Items[todel]

                            for idx2, item2 in enumerate(self.parent.Graph_Items):  # delet assiciated link
                                type2 = item2.dictionnaire["Type"]
                                if type2 in [-100]:
                                    if item2.dictionnaire["noisegainT"] == Name:
                                        exPopup = QuestionRaplaceNoisegainT(self, item=item2, Graph_Items=self.parent.Graph_Items,
                                                                            texte="The pop (for gain and Time constant) for the noise : " + item2.dictionnaire[
                                                                                "Name"] + " doesn't exist anymore\nPlease select another one")
                                        exPopup.exec()
                            break
            else:  # rechercher et supprimer la link la plus proche
                cellId = -1
                length = np.Inf
                for idx, item in enumerate(self.parent.Graph_Items):
                    # print(item)
                    type = item.dictionnaire["Type"]
                    if type in [-200]:
                        position_c = item.dictionnaire["Pos_c"]
                        length_c = ((position.x() - position_c.x()) ** 2 + (position.y() - position_c.y()) ** 2) ** .5
                        if length > length_c:
                            length = length_c
                            cellId = idx
                if length < 100 and not (cellId == -1):
                    del self.parent.Graph_Items[cellId]
            self.UpdateScene()
            self.parent.Button_Rem.setChecked(0)
            self.parent.forundo()
            self.clickedonitem = -1


        elif self.parent.Button_Mod.isChecked() == True or event.modifiers() == Qt.ControlModifier or rightclickinfp == 'Modify':

            position = QPointF(event.pos())
            position = self.mapToScene(position.x(), position.y())
            # print(position)
            findellipse = False
            findellipse_pos = []
            for item in self.scene.items():  # seek for ellipse
                if item.type() == QGraphicsEllipseItem().type():
                    # print(item.contains(position))
                    if item.contains(position):
                        # self.scene.removeItem(item)
                        findellipse = True
                        findellipse_pos = item.boundingRect()
                        # print(findellipse_pos)
                        break
            if findellipse == True:  # e
                for idx, item in enumerate(self.parent.Graph_Items):
                    itemid = item.dictionnaire["ID"]
                    type = item.dictionnaire["Type"]
                    Name = item.dictionnaire["Name"]
                    if type in range(-99, -1) or type in [-100]:
                        position_c = item.dictionnaire["Pos"]
                        if position_c == findellipse_pos.center():
                            self.parent.Button_Mod.setChecked(0)
                            popNames = getNames(self.parent.Graph_Items)
                            exPopup = Questionparam(self, item=item, Graph_Items=self.parent.Graph_Items)
                            # exPopup.show()
                            if exPopup.exec_() == QDialog.Accepted:
                                if not (exPopup.item.dictionnaire['Name'] == Name):

                                    if exPopup.item.dictionnaire['Name'] in popNames:
                                        while exPopup.item.dictionnaire['Name'] in popNames:
                                            text, ok = QInputDialog.getText(self, '', "The  Name '" +
                                                                            exPopup.item.dictionnaire['Name'] +
                                                                            "' Already exists\n Please, enter another one:")
                                            if ok:
                                                exPopup.item.dictionnaire['Name'] = text

                                self.parent.Graph_Items[idx] = exPopup.item

                                for idx2, item2 in enumerate(self.parent.Graph_Items):  # delet assiciated link
                                    type2 = item2.dictionnaire["Type"]
                                    if type2 in [-200]:
                                        if item2.dictionnaire["cellId_e"] == itemid or item2.dictionnaire["cellId_r"] == itemid:
                                            cellName_e = self.parent.Graph_Items[findID(self.parent.Graph_Items, item2.dictionnaire["cellId_e"])].dictionnaire['Name']
                                            cellName_r = self.parent.Graph_Items[findID(self.parent.Graph_Items, item2.dictionnaire["cellId_r"])].dictionnaire['Name']
                                            self.parent.Graph_Items[idx2].dictionnaire['Name'] = cellName_e + '_to_' + cellName_r

                                for idx2, item2 in enumerate(self.parent.Graph_Items):  # delet assiciated link
                                    type2 = item2.dictionnaire["Type"]
                                    if type2 in [-100]:
                                        if item2.dictionnaire["noisegainT"] == Name:
                                            item2.dictionnaire["noisegainT"] = self.parent.Graph_Items[idx].dictionnaire["Name"]

                                print('Accepted')
                            else:
                                print('Cancelled')
                            exPopup.deleteLater()

            else:  # rechercher et supprimer la link la plus proche
                cellId = -1
                length = np.Inf
                for idx, item in enumerate(self.parent.Graph_Items):
                    # print(item)
                    itemid = item.dictionnaire["ID"]
                    type = item.dictionnaire["Type"]
                    if type in [-200]:
                        position_c = item.dictionnaire["Pos_c"]
                        length_c = ((position.x() - position_c.x()) ** 2 + (position.y() - position_c.y()) ** 2) ** .5
                        if length > length_c:
                            length = length_c
                            cellId = idx
                if length < 100:
                    self.parent.Button_Mod.setChecked(0)
                    exPopup = Questionparam(self, item=self.parent.Graph_Items[cellId], Graph_Items=self.parent.Graph_Items)
                    # exPopup.show()
                    if exPopup.exec_() == QDialog.Accepted:
                        # exPopup.editfromlabel()
                        self.parent.Graph_Items[cellId] = exPopup.item
                        print('Accepted')
                    else:
                        print('Cancelled')
                    exPopup.deleteLater()
            self.clickedonitem = -1
            self.UpdateScene()
            self.parent.forundo()
        else:
            cellId = -1
            position = QPointF(event.pos())
            position = self.mapToScene(position.x(), position.y())
            # print(position)
            findellipse = False
            findellipse_pos = []
            for item in self.scene.items():  # seek for ellipse
                if item.type() == QGraphicsEllipseItem().type():
                    # print(item.contains(position))
                    if item.contains(position):
                        # self.scene.removeItem(item)
                        findellipse = True
                        findellipse_pos = item.boundingRect()
                        # print(findellipse_pos)
                        break
            if findellipse == True:  # e
                for idx, item in enumerate(self.parent.Graph_Items):
                    type = item.dictionnaire["Type"]
                    if type in range(-99, -1) or type in [-100]:
                        position_c = item.dictionnaire["Pos"]
                        if position_c == findellipse_pos.center():
                            self.parent.printItem(item)
                            cellId = idx
            else:  # rechercher et supprimer la link la plus proche
                cellId = -1
                length = np.Inf
                for idx, item in enumerate(self.parent.Graph_Items):
                    # print(item)
                    type = item.dictionnaire["Type"]
                    if type in [-200]:
                        position_c = item.dictionnaire["Pos_c"]
                        length_c = ((position.x() - position_c.x()) ** 2 + (position.y() - position_c.y()) ** 2) ** .5
                        if length > length_c:
                            length = length_c
                            cellId = idx
                if length < 100:
                    self.parent.printItem(self.parent.Graph_Items[cellId])
                else:
                    cellId = -1
                    self.positionclickedon = self.mapToScene(event.pos())
            self.clickedonitem = cellId

    # def keyPressEvent(self,event):
    #     event_key =''
    #     if event.text() != "":
    #         event_key = str((event.key())).lower()
    #     print(event_key,event.key(),event.modifiers() == Qt.ControlModifier ,event.modifiers() == Qt.ShiftModifier )
    #     if (event.modifiers() == Qt.ControlModifier or event.modifiers() == Qt.ShiftModifier ) and event.text() != "":
    #             self.myText = "Ctrl+" + event_key
    # print(event)
    # if event.KeyPress:
    #     if(event.modifiers() == Qt.ControlModifier):
    #         print(event.key(),Qt.Key_Plus)
    #         if (event.key() == Qt.Key_Plus):
    #             print(event.key())

    def wheelEvent(self, event):
        if event.angleDelta().y() > 0:
            factor = self.parent.facteurzoom
        else:
            factor = 1 - (self.parent.facteurzoom - 1)
        self.scale(factor, factor)
        self.scene.update()

    def UpdateScene(self, ):
        for item in self.scene.items():
            self.scene.removeItem(item)
        for item in self.parent.Graph_Items:
            itemid = item.dictionnaire["ID"]
            type = item.dictionnaire["Type"]
            nom = item.dictionnaire["Name"]
            position = item.dictionnaire["Pos"]
            if type == -200:  # interneuron inhibiteur
                index_item = findID(self.parent.Graph_Items, item.dictionnaire["cellId_e"])
                color = self.parent.Graph_Items[index_item].dictionnaire['color']
                # typeemeteur = self.parent.Graph_Items[index_item].dictionnaire['Type']
                # print(item,index_item,typeemeteur)
                # if typeemeteur == -2:
                #     color = Qt.red
                # elif typeemeteur == -3:
                #     color = Qt.blue
                # elif typeemeteur == -4:
                #     color = Qt.darkGreen
                # elif typeemeteur == -5:
                #     color = Qt.darkMagenta
                # elif typeemeteur == -100:
                #     color = Qt.white
                # elif typeemeteur == -6:
                #     color = QColor('#FF9900')
                # elif typeemeteur == -7:
                #     color = QColor('#33DDEE')

                type = item.dictionnaire["Type"]
                nom = item.dictionnaire["Name"]
                position2 = item.dictionnaire["Pos2"]
                position_c = item.dictionnaire["Pos_c"]
                Cx = item.dictionnaire["Cx"]

                xsize = 10
                percent = 0

                if item.dictionnaire["cellId_e"] == item.dictionnaire["cellId_r"]:

                    midpoint = (position + position_c) / 2

                    rx = abs(midpoint.x() - position.x())
                    if rx <= 1.:
                        rx = 1.
                    ry = abs(midpoint.y() - position.y())
                    if ry <= 1.:
                        ry = 1.
                    r = (rx ** 2 + ry ** 2) ** 0.5
                    # print((position.x(),position.y()),(rx, ry, r),(position.x(),position.y()-  r),(position.x() + 2*r ,position.y()+  r))
                    path = QPainterPath()
                    path.addEllipse(QPointF(midpoint.x(), midpoint.y()), rx * (r / rx), ry * (r / ry))
                    # rot
                    rx = midpoint.x() - position.x()
                    # if rx<=1.:
                    #     rx=1.
                    ry = midpoint.y() - position.y()
                    # if ry<=1.:
                    #     ry=1.
                    phi1 = [round(np.mod(np.arcsin(ry / r), 2 * np.pi), 3), round(np.mod(np.pi - np.arcsin(ry / r), 2 * np.pi), 3)]
                    phi2 = [round(np.mod(np.arccos(rx / r), 2 * np.pi), 3), round(np.mod(2 * np.pi - np.arccos(rx / r), 2 * np.pi), 3)]

                    phi1 = [np.arcsin(ry / r), np.pi - np.arcsin(ry / r)]
                    for idx, val in enumerate(phi1):
                        if val < 0:
                            phi1[idx] = val + 2 * np.pi
                        phi1[idx] = round(phi1[idx], 1)
                    phi2 = [np.arccos(rx / r), 2 * np.pi - np.arccos(rx / r)]
                    for idx, val in enumerate(phi2):
                        if val < 0:
                            phi2[idx] = val + 2 * np.pi
                        phi2[idx] = round(phi2[idx], 1)

                    if phi1[0] == phi2[0]:
                        phi = phi1[0]
                    elif phi1[0] == phi2[1]:
                        phi = phi1[0]
                    elif phi1[1] == phi2[0]:
                        phi = phi1[1]
                    elif phi1[1] == phi2[1]:
                        phi = phi1[1]
                    else:
                        phi = 0
                    t = QTransform()

                    t.translate(path.boundingRect().center().x(), path.boundingRect().center().y())
                    t.rotate(phi * 180 / np.pi)
                    t.translate(-path.boundingRect().center().x(), -path.boundingRect().center().y())

                    # t.rotate(phi*180/np.pi)
                    path = t.map(path)

                    # path2 = QPainterPath()
                    # pointmidle = path.pointAtPercent(0)
                    # path2.moveTo(pointmidle)
                    # rect = newtext.boundingRect()
                    # newtext.setPos(position_c.x() - rect.width()/2  , position_c.y()-rect.height()/2 )
                    # path2.lineTo(position_c.x() , position_c.y())
                    # self.scene.addPath(path2,QPen(QColor(128, 128, 128, 128),2))

                    arrow = QPolygonF([QPointF(0, 20), QPointF(0, -20), QPointF(20, 0)])
                    t = QTransform()
                    t.rotate(-path.angleAtPercent(0))
                    # t.translate(path.pointAtPercent(0.75))
                    arrow = t.map(arrow)
                    arrow.translate(path.pointAtPercent(0))
                    self.scene.addPolygon(arrow, QPen(color, 5), QBrush(color))

                    self.scene.addPath(path, QPen(color, 5))
                    if self.withouttext:  # without text =0
                        newtext = QGraphicsTextItem(nom + '\nC:' + Cx)
                        font = QFont()
                        font.setPixelSize(20)
                        newtext.setFont(font)
                        rect = newtext.boundingRect()
                        newtext.setPos(position_c.x() - rect.width() / 2, position_c.y() - rect.height() / 2)
                        self.scene.addItem(newtext)
                else:
                    path = QPainterPath()
                    path.moveTo(position.x(), position.y())
                    midpoint = (position + position2) / 2
                    x = 2 * (position_c.x() - midpoint.x()) + midpoint.x()
                    y = 2 * (position_c.y() - midpoint.y()) + midpoint.y()
                    pos = QPointF(x, y)

                    path.quadTo(pos.x(), pos.y(), position2.x(), position2.y())
                    self.scene.addPath(path, QPen(color, 5))

                    # path2 = QPainterPath()
                    # pointmidle = path.pointAtPercent(0.5)
                    # path2.moveTo(pointmidle)
                    # rect = newtext.boundingRect()
                    # newtext.setPos(position_c.x() - rect.width()/2  , position_c.y()-rect.height()/2 )
                    # path2.lineTo(position_c.x() , position_c.y())
                    # self.scene.addPath(path2,QPen(QColor(128, 128, 128, 128),2))

                    arrow = QPolygonF([QPointF(0, 20), QPointF(0, -20), QPointF(20, 0)])
                    t = QTransform()
                    t.rotate(-path.angleAtPercent(0.75))
                    # t.translate(path.pointAtPercent(0.75))
                    arrow = t.map(arrow)
                    ab = 1 - 150. / ((item.dictionnaire['Pos'] - item.dictionnaire['Pos_c']).manhattanLength() + \
                                     (item.dictionnaire['Pos_c'] - item.dictionnaire['Pos2']).manhattanLength())
                    arrow.translate(path.pointAtPercent(0.75))
                    if self.withouttext:  # without text =0
                        newtext = QGraphicsTextItem(nom + '\nC:' + Cx)
                        font = QFont()
                        font.setPixelSize(20)
                        newtext.setFont(font)
                        rect = newtext.boundingRect()
                        newtext.setPos(position_c.x() - rect.width() / 2, position_c.y() - rect.height() / 2)
                        self.scene.addItem(newtext)

                    self.scene.addPolygon(arrow, QPen(color, 5), QBrush(color))

        for item in self.parent.Graph_Items:
            type = item.dictionnaire["Type"]
            nom = item.dictionnaire["Name"]
            position = item.dictionnaire["Pos"]
            Hx = item.dictionnaire["Hx"]
            Tx = item.dictionnaire["Tx"]

            if type in range(-99, -1) or type in [-100]:
                color = item.dictionnaire["color"]
                # if type == -2:
                #     color = Qt.red
                # elif type == -3:
                #     color = Qt.blue
                # elif type == -4:
                #     color = Qt.darkGreen
                # elif type == -5:
                #     color = Qt.darkMagenta
                # elif type == -100:
                #     color = Qt.white
                # elif type == -6:
                #     color = QColor('#FF9900')
                # elif type == -7:
                #     color = QColor('#33DDEE')

                xsize = 150
                ysize = 150
                newitem = QGraphicsEllipseItem(position.x() - xsize / 2., position.y() - ysize / 2., xsize, ysize)
                newitem.setBrush(QBrush(color, style=Qt.SolidPattern))
                self.scene.addItem(newitem)

                if type == -100:
                    mean = item.dictionnaire["mean"]
                    std = item.dictionnaire["std"]
                    gt = item.dictionnaire["noisegainT"]
                    newtext = QGraphicsTextItem(nom + '\nm:' + mean + '\ns:' + std + '\ngain:' + gt)
                else:
                    newtext = QGraphicsTextItem(nom + '\nH:' + Hx + '\nλ:' + Tx)
                # doc = QTextDocument()
                # doc.setDefaultTextOption(QTextOption(Qt.AlignCenter))
                # newtext.setDocument(doc)
                # textItem.document()->setDefaultTextOption(QTextOption(Qt::AlignCenter | Qt::AlignVCenter))
                if self.withouttext:  # without text =0
                    font = QFont()
                    font.setPixelSize(20)
                    newtext.setFont(font)
                    rect = newtext.boundingRect()
                    newtext.setPos(position.x() - rect.width() / 2, position.y() - rect.height() / 2)
                    self.scene.addItem(newtext)


class new_item_to_plot:
    def __init__(self, ID=None, Type=None, Name=None, Pos=None, Pos2=None, Pos_c=None, cellId_e=None, cellId_r=None, Hx=None, Tx=None, Cx=None,
                 e0x=None, v0x=None, rx=None, mean=None, std=None, noisegainT=None, color=None):  # Notre méthode constructeur
        """Pour l'instant, on ne va définir qu'un seul attribut"""
        self.dictionnaire = dict()
        self.dictionnaire["ID"] = ID
        self.dictionnaire["Type"] = Type
        self.dictionnaire["Name"] = Name
        self.dictionnaire["Pos"] = Pos
        self.dictionnaire["Pos2"] = Pos2
        self.dictionnaire["Pos_c"] = Pos_c
        self.dictionnaire["cellId_e"] = cellId_e
        self.dictionnaire["cellId_r"] = cellId_r
        self.dictionnaire["color"] = color
        self.dictionnaire["e0x"] = e0x
        self.dictionnaire["v0x"] = v0x
        self.dictionnaire["rx"] = rx
        self.dictionnaire["Hx"] = Hx
        self.dictionnaire["Tx"] = Tx
        self.dictionnaire["Cx"] = Cx
        self.dictionnaire["mean"] = mean
        self.dictionnaire["std"] = std
        self.dictionnaire["eq1"] = None
        self.dictionnaire["eq2"] = None
        self.dictionnaire["lfp"] = None
        self.dictionnaire["noise"] = None
        self.dictionnaire["linknoise_r"] = None
        self.dictionnaire["linkId_r"] = None
        self.dictionnaire["Hname"] = None
        self.dictionnaire["Tname"] = None
        self.dictionnaire["Cname"] = None
        self.dictionnaire["e0name"] = None
        self.dictionnaire["v0name"] = None
        self.dictionnaire["rname"] = None
        self.dictionnaire["meanname"] = None
        self.dictionnaire["stdname"] = None
        self.dictionnaire["sigmname"] = None
        self.dictionnaire["noisename"] = None
        self.dictionnaire["noisevarname"] = None
        self.dictionnaire["noisegainT"] = noisegainT
        self.dictionnaire["pulseName"] = None
        self.dictionnaire["ppsName"] = None


class QuestionRaplaceNoisegainT(QDialog):
    def __init__(self, parent=None, item=None, Graph_Items=None, texte=''):
        super(QuestionRaplaceNoisegainT, self).__init__(parent)
        self.parent = parent
        self.Graph_Items = Graph_Items
        self.Param_box = QGroupBox(texte)
        self.Param_box.setFixedWidth(300)
        self.layout_Param_box = QVBoxLayout()
        self.item = item

        label = ['Noise H/λ']
        edit = [['', findPopNale(self.Graph_Items)]]
        widg, Edit = Layout_groupbox_Label_Edit("Parameters", label, edit, width=285, height_add=20, height_per_line=30)

        self.noise_GTe = Edit[0]

        self.horizontalGroupBox_Actions = QGroupBox("Actions")
        self.horizontalGroupBox_Actions.setFixedSize(285, 80)
        self.layout_Actions = QHBoxLayout()
        self.Button_Ok = QPushButton('Ok')
        self.Button_Cancel = QPushButton('Cancel')
        self.Button_Ok.setFixedSize(66, 30)
        self.Button_Cancel.setFixedSize(66, 30)
        self.layout_Actions.addWidget(self.Button_Ok)
        self.layout_Actions.addWidget(self.Button_Cancel)
        self.horizontalGroupBox_Actions.setLayout(self.layout_Actions)
        label = QLabel(texte)
        self.layout_Param_box.addWidget(label)
        self.layout_Param_box.addWidget(widg)
        self.layout_Param_box.addWidget(self.horizontalGroupBox_Actions)
        self.Param_box.setLayout(self.layout_Param_box)
        self.setLayout(self.layout_Param_box)

        self.Button_Ok.clicked.connect(self.myaccept)
        self.Button_Cancel.clicked.connect(self.reject)

    def myaccept(self):
        self.item.dictionnaire["noisegainT"] = self.noise_GTe.currentText()
        self.accept()


class Questionparam(QDialog):
    def __init__(self, parent=None, item=None, Graph_Items=None):
        super(Questionparam, self).__init__(parent)
        self.parent = parent
        self.Graph_Items = Graph_Items
        self.Param_box = QGroupBox("Parameters")
        self.Param_box.setFixedWidth(300)
        self.layout_Param_box = QVBoxLayout()
        self.item = item

        label = ['Name', '2e0', 'v0', 'r', 'Hx', 'λx', 'Cx', 'Noise Mean', 'Noise std', 'Noise H/λ', 'Color']
        edit = self.labelfromedit()
        widg, Edit = Layout_groupbox_Label_Edit("Parameters", label, edit, width=285, height_add=20, height_per_line=30)

        self.Name_edit = Edit[0]
        self.e0x_edit = Edit[1]
        self.v0x_edit = Edit[2]
        self.rx_edit = Edit[3]
        self.Hx_edit = Edit[4]
        self.Tx_edit = Edit[5]
        self.Cx_edit = Edit[6]
        self.mean_edit = Edit[7]
        self.std_edit = Edit[8]
        self.noise_GTe = Edit[9]
        self.Colorselection = Edit[10]

        type = item.dictionnaire['Type']
        if type in range(-99, -1):
            self.Name_edit.setDisabled(0)
            self.e0x_edit.setDisabled(0)
            self.v0x_edit.setDisabled(0)
            self.rx_edit.setDisabled(0)
            self.Hx_edit.setDisabled(0)
            self.Tx_edit.setDisabled(0)
            self.Cx_edit.setDisabled(1)
            self.mean_edit.setDisabled(1)
            self.std_edit.setDisabled(1)
            self.noise_GTe.setDisabled(1)
            self.Colorselection.setDisabled(0)

        elif type in [-200]:
            self.Name_edit.setDisabled(1)
            self.e0x_edit.setDisabled(1)
            self.v0x_edit.setDisabled(1)
            self.rx_edit.setDisabled(1)
            self.Hx_edit.setDisabled(1)
            self.Tx_edit.setDisabled(1)
            self.Cx_edit.setDisabled(0)
            self.mean_edit.setDisabled(1)
            self.std_edit.setDisabled(1)
            self.noise_GTe.setDisabled(0)
            self.Colorselection.setDisabled(1)
        elif type in [-100]:
            self.Name_edit.setDisabled(0)
            self.e0x_edit.setDisabled(1)
            self.v0x_edit.setDisabled(1)
            self.rx_edit.setDisabled(1)
            self.Hx_edit.setDisabled(1)
            self.Tx_edit.setDisabled(1)
            self.Cx_edit.setDisabled(1)
            self.mean_edit.setDisabled(0)
            self.std_edit.setDisabled(0)
            self.Colorselection.setDisabled(0)

        self.horizontalGroupBox_Actions = QGroupBox("Actions")
        self.horizontalGroupBox_Actions.setFixedSize(285, 80)
        self.layout_Actions = QHBoxLayout()
        self.Button_Ok = QPushButton('Ok')
        self.Button_Cancel = QPushButton('Cancel')
        self.Button_Ok.setFixedSize(66, 30)
        self.Button_Cancel.setFixedSize(66, 30)
        self.layout_Actions.addWidget(self.Button_Ok)
        self.layout_Actions.addWidget(self.Button_Cancel)
        self.horizontalGroupBox_Actions.setLayout(self.layout_Actions)

        self.layout_Param_box.addWidget(widg)
        self.layout_Param_box.addWidget(self.horizontalGroupBox_Actions)
        self.Param_box.setLayout(self.layout_Param_box)
        self.setLayout(self.layout_Param_box)

        self.Button_Ok.clicked.connect(self.myaccept)
        self.Button_Cancel.clicked.connect(self.reject)

    def myaccept(self):
        self.editfromlabel()
        self.accept()

    def labelfromedit(self):
        label = [self.item.dictionnaire['Name'],
                 self.item.dictionnaire['e0x'],
                 self.item.dictionnaire['v0x'],
                 self.item.dictionnaire['rx'],
                 self.item.dictionnaire['Hx'],
                 self.item.dictionnaire['Tx'],
                 self.item.dictionnaire['Cx'],
                 self.item.dictionnaire['mean'],
                 self.item.dictionnaire['std'],
                 [self.item.dictionnaire["noisegainT"], findPopNale(self.Graph_Items)],
                 self.item.dictionnaire['color']]
        return label

    def editfromlabel(self):
        self.item.dictionnaire['Name'] = self.Name_edit.text()
        self.item.dictionnaire['e0x'] = self.e0x_edit.text().replace(',', '.')
        self.item.dictionnaire['v0x'] = self.v0x_edit.text().replace(',', '.')
        self.item.dictionnaire['rx'] = self.rx_edit.text().replace(',', '.')
        self.item.dictionnaire['Hx'] = self.Hx_edit.text().replace(',', '.')
        self.item.dictionnaire['Tx'] = self.Tx_edit.text().replace(',', '.')
        self.item.dictionnaire['Cx'] = self.Cx_edit.text().replace(',', '.')
        self.item.dictionnaire['mean'] = self.mean_edit.text().replace(',', '.')
        self.item.dictionnaire['std'] = self.std_edit.text().replace(',', '.')
        self.item.dictionnaire["noisegainT"] = self.noise_GTe.currentText()
        self.item.dictionnaire["color"] = QColor(self.Colorselection.palette().button().color())


class InfoTable(QDialog):
    def __init__(self, parent=None, Graph_Items=None):
        super(InfoTable, self).__init__(parent)
        self.parent = parent
        self.Graph_Items = Graph_Items
        self.setMinimumSize(800, 600)
        self.Param_box = QWidget()
        self.layout_Param_box = QVBoxLayout()

        self.tableWidget = QTableWidget()
        self.tableWidget.setRowCount(len(Graph_Items) + 10)
        nbcol = 7
        self.tableWidget.setColumnCount(nbcol)
        self.tableWidget.setSpan(0, 0, 1, nbcol)

        # population
        self.tableWidget.setItem(0, 0, QTableWidgetItem("Populations:"))
        col = 0
        raw = 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("Name"))
        col += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("Hx"))
        col += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("Tx"))
        col += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("e0x"))
        col += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("v0x"))
        col += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("rx"))
        col += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("color"))

        for item in Graph_Items:
            if item.dictionnaire["Type"] in range(-99, -1):
                col = 0
                raw += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem(str(item.dictionnaire["Name"])))
                col += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem(str(item.dictionnaire["Hx"])))
                col += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem(str(item.dictionnaire["Tx"])))
                col += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem(str(item.dictionnaire["e0x"])))
                col += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem(str(item.dictionnaire["v0x"])))
                col += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem(str(item.dictionnaire["rx"])))
                col += 1
                colorbut = QPushButton()
                set_QPushButton_background_color(colorbut, item.dictionnaire["color"])
                self.tableWidget.setCellWidget(raw, col, colorbut)

        # Noises
        col = 0
        raw += 1
        self.tableWidget.setSpan(raw, col, 1, nbcol)
        raw += 1
        self.tableWidget.setSpan(raw, col, 1, nbcol)
        self.tableWidget.setItem(raw, col, QTableWidgetItem("Noises:"))
        raw += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("Name"))
        col += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("mean"))
        col += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("std"))
        col += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("noisegainT"))
        col += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("color"))

        for item in Graph_Items:
            if item.dictionnaire["Type"] == -100:
                col = 0
                raw += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem(str(item.dictionnaire["Name"])))
                col += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem(str(item.dictionnaire["mean"])))
                col += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem(str(item.dictionnaire["std"])))
                col += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem(str(item.dictionnaire["noisegainT"])))
                col += 1
                colorbut = QPushButton()
                set_QPushButton_background_color(colorbut, item.dictionnaire["color"])
                self.tableWidget.setCellWidget(raw, col, colorbut)

        # Noises
        col = 0
        raw += 1
        self.tableWidget.setSpan(raw, col, 1, nbcol)
        raw += 1
        self.tableWidget.setSpan(raw, col, 1, nbcol)
        self.tableWidget.setItem(raw, col, QTableWidgetItem("Links:"))
        col = 0
        raw += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("Name"))
        col += 1
        self.tableWidget.setItem(raw, col, QTableWidgetItem("Cx"))

        for item in Graph_Items:
            if item.dictionnaire["Type"] == -200:
                col = 0
                raw += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem('C_' + str(item.dictionnaire["Name"])))
                color_e = QColor(Graph_Items[findID(Graph_Items, item.dictionnaire['cellId_e'])].dictionnaire['color'].name())
                color_r = QColor(Graph_Items[findID(Graph_Items, item.dictionnaire['cellId_r'])].dictionnaire['color'].name())

                gradient = QLinearGradient(0, 0, 100, 0)
                gradient.setColorAt(0.0, color_e)
                gradient.setColorAt(1.0, color_r)
                brush = QBrush(gradient)

                self.tableWidget.item(raw, col).setBackground(brush)

                col += 1
                self.tableWidget.setItem(raw, col, QTableWidgetItem(str(item.dictionnaire["Cx"])))

        scroll = QScrollArea()
        widget = QWidget(self)
        widget.setLayout(QHBoxLayout())
        widget.layout().addWidget(self.tableWidget)
        scroll.setWidget(widget)
        scroll.setWidgetResizable(True)

        self.Button_Ok = QPushButton('Close')
        self.Button_Ok.setFixedSize(66, 30)
        self.Button_Ok.clicked.connect(self.accept)

        self.layout_Param_box.addWidget(scroll)

        self.layout_Param_box.addWidget(self.Button_Ok)
        self.Param_box.setLayout(self.layout_Param_box)
        self.setLayout(self.layout_Param_box)


def findID(Graph_Items=None, ID=None):
    for idx, item in enumerate(Graph_Items):
        if item.dictionnaire['ID'] == ID:
            return idx


def findName(Graph_Items=None, Name=None):
    for idx, item in enumerate(Graph_Items):
        if item.dictionnaire['Name'] == Name:
            return idx


def getNames(Graph_Items=None):
    Names = []
    for idx, item in enumerate(Graph_Items):
        if item.dictionnaire['Type'] in range(-100, -1):
            Names.append(item.dictionnaire['Name'])
    return Names


def findPopNale(Graph_Items=None):
    Names = ['']
    for idx, item in enumerate(Graph_Items):
        if item.dictionnaire['Type'] in range(-99, -1):
            Names.append(item.dictionnaire['Name'])
    return Names


def main():
    app = QApplication(sys.argv)
    app.setStyle('Windows')
    ex = SurfViewer()
    ex.setWindowTitle('Neural Mass Model Generator')
    ex.showMaximized()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
