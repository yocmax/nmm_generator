# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['NMM_Generator2.py'],
             pathex=['C:\\Users\\maxime\\Desktop\\SESAME\\PycharmProjects\\NMM_Applications\\NMM_Generator_pourpapier'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

a.datas += [('add.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\add.png', 'DATA')]
a.datas += [('edit.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\edit.png', 'DATA')]
a.datas += [('exit.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\exit.png', 'DATA')]
a.datas += [('GUI.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\GUI.png', 'DATA')]
a.datas += [('info.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\info.png', 'DATA')]
a.datas += [('new.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\new.png', 'DATA')]
a.datas += [('open.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\open.png', 'DATA')]
a.datas += [('process.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\process.png', 'DATA')]
a.datas += [('redo-arrow.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\redo-arrow.png', 'DATA')]
a.datas += [('remove.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\remove.png', 'DATA')]
a.datas += [('resize.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\resize.png', 'DATA')]
a.datas += [('save.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\save.png', 'DATA')]
a.datas += [('undo-arrow.png',r'C:\Users\maxime\Desktop\SESAME\PycharmProjects\NMM_Applications\NMM_Generator_pourpapier\icons\undo-arrow.png', 'DATA')]	 		 
			 
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='NMM_Generator',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False )
